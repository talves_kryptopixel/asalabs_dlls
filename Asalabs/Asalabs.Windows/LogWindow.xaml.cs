﻿using System;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Asalabs.Windows
{
    /// <summary>
    /// Interaction logic for LogWindow.xaml
    /// </summary>
    public partial class LogWindow
    {
        public TextBox GetTextBox
        {
            get { return TbxLog; }
        }
        public LogWindow()
        {
            InitializeComponent();
        }

        public void Write(string text)
        {
            var s = DateTime.Now.ToString(@"yyyy-MM-dd HH\:mm\:ss") + " - " + text + "\n";
            Action write = (() =>
            {
                TbxLog.AppendText(s);
                TbxLog.CaretIndex = TbxLog.Text.Length;
                TbxLog.ScrollToEnd();
            });
            Application.Current.Dispatcher.BeginInvoke(write);
        }

        private void BtClear_Click(object sender, RoutedEventArgs e)
        {
            TbxLog.Clear();
        }

        private void BtExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        public class LogStreamWriter : TextWriter
        {
            private readonly TextBox _output = null;

            public LogStreamWriter(TextBox output)
            {
                _output = output;
            }

            public override void Write(char value)
            {
                base.Write(value);
                _output.AppendText(value.ToString());
                // When character data is written, append it to the text box.
            }

            public override Encoding Encoding
            {
                get { return System.Text.Encoding.UTF8; }
            }
        }
    }
}
