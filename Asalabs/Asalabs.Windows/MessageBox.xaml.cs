﻿using System.Windows;

namespace Asalabs.Windows
{
    /// <summary>
    /// Interaction logic for MessageBox.xaml
    /// </summary>
    internal partial class MessageBoxWindow
    {
        public string MessageBoxTitle
        {
            set { MessageBox.Title = value; }
            get { return MessageBox.Title; }
        }
        public string MessageBoxMessage
        {
            set { TbkMessage.Text = value; }
            get { return TbkMessage.Text; }
        }
        internal MessageBoxWindow()
        {
            InitializeComponent();
            Topmost = true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }

    public static class MessageBox
    {
        public static void Show(string message)
        {
            var mb = new MessageBoxWindow {MessageBoxMessage = message};
            mb.Show();
        }
        public static void ShowDialog(string message)
        {
            var mb = new MessageBoxWindow { MessageBoxMessage = message };
            mb.ShowDialog();
        }
        public static void Show(string message, string title)
        {
            var mb = new MessageBoxWindow { MessageBoxMessage = message, MessageBoxTitle = title };
            mb.Show();
        }
        public static void ShowDialog(string message, string title)
        {
            var mb = new MessageBoxWindow { MessageBoxMessage = message, MessageBoxTitle = title };
            mb.ShowDialog();
        }
    }
}
