﻿using System.Collections.Generic;
using System.Net;

namespace Asalabs.ControlProtocol.ElementOne
{
    public class Versis : ElementOneBase
    {
        internal readonly Dictionary<Command, string> CommandList = new Dictionary<Command, string>();

        public int Id { set; get; }
        public void InicializeComponents(IPAddress ip)
        {
            InicializeDictionary();
            InicializeTcp(ip);
        }

        public enum Command
        {
            MoveUp,
            MoveDown,
            DisplayOn,
            DisplayOff,
            Stop,
            Status,
            DeviceDetails,
            Protocol,
            Check,
            LockOn,
            LockOff,
            LockPermOn,
            LockPermOff
        }
        private void InicializeDictionary()
        {
            CommandList.Add(Command.Check, "check");
            CommandList.Add(Command.DeviceDetails, "device details");
            CommandList.Add(Command.DisplayOff, "display off");
            CommandList.Add(Command.DisplayOn, "display on");
            CommandList.Add(Command.LockOff, "lock off");
            CommandList.Add(Command.LockOn, "lock on");
            CommandList.Add(Command.LockPermOff, "lock perm off");
            CommandList.Add(Command.LockPermOn, "lock perm on");
            CommandList.Add(Command.MoveDown, "move down");
            CommandList.Add(Command.MoveUp, "move up");
            CommandList.Add(Command.Protocol, "protocol");
            CommandList.Add(Command.Status, "status");
            CommandList.Add(Command.Stop, "stop");
        }

        public bool Send(Command c)
        {
            string cmd;
            CommandList.TryGetValue(c, out cmd);
            return SendCommand(cmd);
        }
    }
}
