﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Asalabs.Extensions;

namespace Asalabs.ControlProtocol.ElementOne
{
    public class ElementOneBase
    {
        public event EventHandler<StringArgs> Error;

        public void SendError(string error)
        {
            if (Error != null)
                Error(this, new StringArgs {Value = error});
        }
        public event EventHandler<StringArgs> DataReceived;

        public void SendData(string data)
        {
            if (DataReceived != null)
                DataReceived(this, new StringArgs { Value = data });
        }

        internal TcpClient Tcp;
        internal IPAddress Ip { set; get; }

        internal void InicializeTcp(IPAddress ip)
        {
            try
            {
                Ip = ip;
                Tcp = new TcpClient(AddressFamily.InterNetwork);
                Tcp.BeginConnect(ip, 8282, WaitForData, null);
            }
            catch (Exception ex)
            {
                SendError(ex.Message);
            }
        }

        internal void WaitForData(IAsyncResult result)
        {
            try
            {
                var stream = Tcp.GetStream();
                var buffer = new byte[128];
                stream.BeginRead(buffer, 0, buffer.Length, DataArrived, buffer);

            }
            catch (Exception ex)
            {
                SendError(ex.Message);
            }
        }

        private void DataArrived(IAsyncResult result)
        {
            try
            {
                var buffer = result.AsyncState as byte[];
                if (buffer != null)
                {
                    var res = Encoding.ASCII.GetString(buffer, 0, buffer.Length);
                    SendData(res);
                }
            }
            catch (Exception ex)
            {
                SendError(ex.Message);
            }
            WaitForData(null);
        }

        internal bool SendCommand(string cmd)
        {
            if (string.IsNullOrEmpty(cmd)) return false;
            var bytes = Encoding.ASCII.GetBytes(cmd + "\r\n");
            var stream = Tcp.GetStream();
            stream.Write(bytes,0,bytes.Length);
            return true;
        }

        /*
         public class ElementOneBase
    {
        internal static Socket Tcp;
        internal IPAddress Ip { set; get; }
        private Thread BackgroundThread;
        internal void InicializeTcp(IPAddress ip)
        {
            Tcp = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            Tcp.Connect(ip, 8282);
            BackgroundThread = new Thread(WaitForData){IsBackground = true};
            BackgroundThread.Start();
        }

        internal void WaitForData()
        {
            Tcp.BeginReceive(,DataReceived);
        }

        AsyncCallback DataReceived()
        {
            
        }
        
        internal bool SendCommand(string cmd)
        {
            if (string.IsNullOrEmpty(cmd)) return false;
            var bytes = Encoding.ASCII.GetBytes(cmd + "\r\n");
            Tcp.Send(bytes);
            return true;
        }
    }
         */
    }
}
