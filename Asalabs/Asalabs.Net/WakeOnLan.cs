﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace Asalabs.Net
{
    public class WakeOnLan
    {

        public class MACAddress
        {
            private byte[] bytes;

            public MACAddress(byte[] bytes)
            {
                if (bytes.Length != 6)
                    throw new System.ArgumentException("MAC address must have 6 bytes");
                this.bytes = bytes;
            }

            public MACAddress(string s)
            {
                byte[] b = StringToByteArray(s);
                if (b.Length != 6)
                    throw new System.ArgumentException("MAC address must have 6 bytes");
                this.bytes = b;
            }
            public byte this[int i]
            {
                get { return this.bytes[i]; }
                set { this.bytes[i] = value; }
            }

            public override string ToString()
            {
                return BitConverter.ToString(this.bytes, 0, 6);
            }
        }

        public static void WakeUp(MACAddress mac)
        {
            byte[] packet = new byte[17 * 6];

            for (int i = 0; i < 6; i++)
                packet[i] = 0xff;

            for (int i = 1; i <= 16; i++)
                for (int j = 0; j < 6; j++)
                    packet[i * 6 + j] = mac[j];

            UdpClient client = new UdpClient();
            client.Connect(IPAddress.Broadcast, 0);
            client.Send(packet, packet.Length);
        }

        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length).Where(x => x % 2 == 0).Select(x => Convert.ToByte(hex.Substring(x, 2), 16)).ToArray();
        }

    }
}
