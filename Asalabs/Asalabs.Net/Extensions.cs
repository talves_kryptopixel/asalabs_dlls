﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Asalabs.Net
{
    public static class Extensions
    {
        public static bool ValidateMacAddressChars(string s)
        {
            var regex = new Regex(@"^[A-Fa-f0-9\-\:]*$");
            var isHex = regex.IsMatch(s);
            var isSeparator = (s == "-");
            return isHex || isSeparator;
        }
        /// <summary>
        /// Format a string to MacAddress type xx-xx-xx-xx-xx-xx
        /// </summary>
        /// <param name="mac"></param>
        /// <returns></returns>
        public static string ToFormatedMacAddress(this string mac)
        {
            int t;
            return mac.ToFormatedMacAddress(out t);
        }
        /// <summary>
        /// Format a string to MacAddress type xx-xx-xx-xx-xx-xx
        /// </summary>
        /// <param name="mac"></param>
        /// <param name="separatorAddedCount">number of added '-'</param>
        /// <returns></returns>
        public static string ToFormatedMacAddress(this string mac, out int separatorAddedCount)
        {
            var res = string.Empty;
            var idxBefore = mac.Length - mac.Replace("-", "").Length;
            var simple = mac.Replace("-", "");
            var max = simple.Length;
            if (max > 12) max = 12;
            simple = simple.Substring(0, max);
            var hexList = new List<string>();

            while (simple.Length > 2)
            {
                hexList.Add(simple.Substring(0, 2));
                simple = simple.Substring(2, simple.Length - 2);
            }
            hexList.Add(simple);
            res = hexList.Aggregate(res, (current, hex) => current + (hex + "-"));
            var li = res.LastIndexOf('-');
            if (li == res.Length - 1)
                res = res.Remove(li, 1);
            var idxAfter = res.Length - res.Replace("-", "").Length;
            separatorAddedCount = idxAfter - idxBefore;
            return res;
        }
    }
}
