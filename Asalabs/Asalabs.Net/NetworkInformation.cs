﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;

namespace Asalabs.Net
{
    public static class NetworkInformation
    {

        public static List<PhysicalAddress> GetEthernetMacAddressList()
        {
            var nics = NetworkInterface.GetAllNetworkInterfaces().ToList();
            var ethernetAdapters = nics.Where(c => c.NetworkInterfaceType == NetworkInterfaceType.Ethernet).ToList();
            return ethernetAdapters.Select(adp => adp.GetPhysicalAddress()).ToList();
        }
        public static List<PhysicalAddress> GetGigabitEthernetMacAddressList()
        {
            var nics = NetworkInterface.GetAllNetworkInterfaces().ToList();
            var ethernetAdapters = nics.Where(c => c.NetworkInterfaceType == NetworkInterfaceType.Ethernet && c.Description.Contains("Gigabit")).ToList();
            return ethernetAdapters.Select(adp => adp.GetPhysicalAddress()).ToList();
        }

        public static string ToFormatedString(this PhysicalAddress fa)
        {
            var result = string.Empty;
            var bytes = fa.GetAddressBytes();
            result = bytes.Aggregate(result, (current, b) => current + (b.ToString("X2") + "-"));
            return result.Length>1 ? result.Substring(0,result.Length-1) : string.Empty;
        }

        public static List<NetworkInterface> ShowNetworkInterfaces()
        {
            var computerProperties = IPGlobalProperties.GetIPGlobalProperties();
            var nics = NetworkInterface.GetAllNetworkInterfaces().ToList();

            Console.WriteLine("Interface information for {0}.{1}     ",
                    computerProperties.HostName, computerProperties.DomainName);
            if (nics.Count < 1)
            {
                Console.WriteLine("  No network interfaces found.");
                return nics;
            }

            Console.WriteLine("  Number of interfaces .................... : {0}", nics.Count);
            foreach (var adapter in nics)
            {
                var properties = adapter.GetIPProperties(); //  .GetIPInterfaceProperties();
                Console.WriteLine();
                Console.WriteLine(adapter.Description);
                Console.WriteLine(String.Empty.PadLeft(adapter.Description.Length, '='));
                Console.WriteLine("  Interface type .......................... : {0}", adapter.NetworkInterfaceType);
                Console.Write("  Physical address ........................ : ");
                var address = adapter.GetPhysicalAddress();
                byte[] bytes = address.GetAddressBytes();
                for (int i = 0; i < bytes.Length; i++)
                {
                    // Display the physical address in hexadecimal.
                    Console.Write("{0}", bytes[i].ToString("X2"));
                    // Insert a hyphen after each byte, unless we are at the end of the  
                    // address. 
                    if (i != bytes.Length - 1)
                    {
                        Console.Write("-");
                    }
                }
                Console.WriteLine();
            }
            return nics;
        }
    }
}
