﻿using System.Net.NetworkInformation;
using System.Text;

namespace Asalabs.Net
{
    public static class PingHost
    {
        private static readonly Ping PingSender = new Ping();
        private static readonly PingOptions Options = new PingOptions();

        public static PingReply Ping(string host)
        {
            // Use the default Ttl value which is 128, 
            // but change the fragmentation behavior.
            Options.DontFragment = true;

                // Create a buffer of 32 bytes of data to be transmitted. 
            const string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            var buffer = Encoding.ASCII.GetBytes(data);
            const int timeout = 20;
            return PingSender.Send(host, timeout, buffer, Options);
        }
    }
}



/*    public class PingExample
    {
        // args[0] can be an IPaddress or host name. 
        public static void Main (string[] args)
        {
            Ping pingSender = new Ping ();
            PingOptions options = new PingOptions ();

            // Use the default Ttl value which is 128, 
            // but change the fragmentation behavior.
            options.DontFragment = true;

            // Create a buffer of 32 bytes of data to be transmitted. 
            string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            byte[] buffer = Encoding.ASCII.GetBytes (data);
            int timeout = 120;
            PingReply reply = pingSender.Send (args[0], timeout, buffer, options);
            if (reply.Status == IPStatus.Success)
            {
                Console.WriteLine ("Address: {0}", reply.Address.ToString ());
                Console.WriteLine ("RoundTrip time: {0}", reply.RoundtripTime);
                Console.WriteLine ("Time to live: {0}", reply.Options.Ttl);
                Console.WriteLine ("Don't fragment: {0}", reply.Options.DontFragment);
                Console.WriteLine ("Buffer size: {0}", reply.Buffer.Length);
            }
        }
    }*/