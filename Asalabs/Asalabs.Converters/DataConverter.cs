﻿using System;
using System.Collections.Generic;
using System.IO.Ports;

namespace Asalabs.Converters
{
    public enum DataBits { Five = 5, Six = 6, Seven = 7, Eight = 8 };

    public class DataConvert
    {
        public static Parity StringToParity(string s)
        {
            return (Parity)Enum.Parse(typeof(Parity), s);
        }

        public static StopBits StringToStopBits(string s)
        {
            return (StopBits)Enum.Parse(typeof(StopBits), s);
        }

        public static Handshake StringToHandshake(string s)
        {
            return (Handshake)Enum.Parse(typeof(Handshake), s);
        }

        public byte[] HexStringToByteArray(String hexString)
        {
            try
            {
                var numberChars = hexString.Length;
                var bytes = new byte[numberChars / 2];
                for (var i = 0; i < numberChars; i += 2)
                {
                    bytes[i / 2] = Convert.ToByte(hexString.Substring(i, 2), 16);
                } return bytes;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public byte HexToByte(string s)
        {
            try
            {
                if (s.Length >= 2)
                    return Convert.ToByte(s.Substring(0, 2), 16);
                else
                    return 0;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public string ByteArrayToString(byte[] byteArray, int arrayLenght)
        {
            try
            {
                var s = "";
                int j = 0;
                for (int i = 0; i < arrayLenght; i++)
                {
                    j = ((int)byteArray[i]);
                    string aux = j.ToString("X");
                    if (aux.Length == 2)
                        s += aux;
                    else
                        s += "0" + aux;
                }
                return s;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public string IntListToString(List<int> list)
        {
            try
            {
                string s = "";
                if (list != null)
                    foreach (int i in list)
                        s += i.ToString() + " ";
                return s;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}