﻿using System;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Windows.Media;
using System.Windows;
using Asalabs.ErrorReport;
using Brush = System.Windows.Media.Brush;
using Color = System.Windows.Media.Color;
using ColorConverter = System.Windows.Media.ColorConverter;

namespace Asalabs.Converters
{
    public static class Converter
    {
        public static string ImageToString(Image img)
        {
            try
            {
                var ms = new MemoryStream();

                img.Save(ms, img.RawFormat);

                var array = ms.ToArray();

                return Convert.ToBase64String(array);
            }
            catch (Exception ex)
            {
                Errors.Show("ImageToString", ex, Errors.ErrorType.Warning);
            }
            return null;
        }

        public static Image StringToImage(string imageString)
        {
            try
            {
                var array = Convert.FromBase64String(imageString);

                var image = Image.FromStream(new MemoryStream(array));

                return image;
            }
            catch (Exception ex)
            {
                Errors.Show("StringToImage", ex, Errors.ErrorType.Warning);
            }
            return null;
        }


        public static string EncodeToString(byte[] data)
        {
            string res = null;
            try
            {
                res = Convert.ToBase64String(data);
            }
            catch (Exception ex)
            {
                Errors.Show("EncodeToString", ex, Errors.ErrorType.Warning);
            }
            return res;
        }

        public static byte[] DecodeFromString(string data)
        {
            byte[] res = null;

            try
            {
                res = Convert.FromBase64String(data);
            }
            catch (Exception ex)
            {
                Errors.Show("DecodeFromString", ex, Errors.ErrorType.Warning);
            }
            return res;
        }
        
        public static byte[] HexToByteArray(String hexString)
        {
            try
            {
                var numberChars = hexString.Length;
                var bytes = new byte[numberChars / 2];
                for (var i = 0; i < numberChars; i += 2)
                {
                    bytes[i / 2] = Convert.ToByte(hexString.Substring(i, 2), 16);
                } return bytes;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static byte HexToByte(string hex)
        {
            try
            {
                if (hex.Length >= 2)
                    return Convert.ToByte(hex.Substring(0, 2), 16);
                else
                    return 0;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static string ByteArrayToHex(byte[] byteArray, int arrayLenght)
        {
            try
            {
                var s = String.Empty;
                var j = 0;
                for (var i = 0; i < arrayLenght; i++)
                {
                    j = ((int)byteArray[i]);
                    string aux = j.ToString("X");
                    if (aux.Length == 2)
                        s += aux;
                    else
                        s += "0" + aux;
                }
                return s;
            }
            catch (Exception)
            {
                return null;
            }
        }
        
        public static string DecToHex(int dec, int minHexLength)
        {
            var hex = dec.ToString("X");
            while (hex.Length < minHexLength)
                hex = "0" + hex;
            return hex;
        }

        public static string ByteToString(byte[] b)
        {
            var ue = new UnicodeEncoding();
            var str = ue.GetString(b);
            return str;
        }
        
        public static byte[] StringToByte(string s)
        {
            var ue = new UnicodeEncoding();
            var b = ue.GetBytes(s);
            return b;
        }

        public static Brush StringToBrush(string brush)
        {
            Brush br = null;
            if (string.IsNullOrEmpty(brush)) return null;
            if (brush[0] == '#')
            {
                if (brush.Length == 9)
                {
                    var a = HexToByte(brush.Substring(1, 2));
                    var r = HexToByte(brush.Substring(3, 2));
                    var g = HexToByte(brush.Substring(5, 2));
                    var b = HexToByte(brush.Substring(7, 2));
                    br = new SolidColorBrush(Color.FromArgb(a, r, g, b));
                    return br;
                }
            }
            else
            {
                var convertFromString = ColorConverter.ConvertFromString(brush);
                if (convertFromString != null)
                    br = new SolidColorBrush((Color)convertFromString);
                return br;
            }
            return null;
        }

        public static FontWeight StringToFontWeight(string font)
        {
            switch (font)
            {
                case "Black":
                    return FontWeights.Black;
                case "Bold":
                    return FontWeights.Bold;
                case "DemiBold":
                    return FontWeights.DemiBold;
                case "ExtraBlack":
                    return FontWeights.ExtraBlack;
                case "ExtraBold":
                    return FontWeights.ExtraBold;
                case "ExtraLight":
                    return FontWeights.ExtraLight;
                case "Heavy":
                    return FontWeights.Heavy;
                case "Light":
                    return FontWeights.Light;
                case "Medium":
                    return FontWeights.Medium;
                case "Normal":
                    return FontWeights.Normal;
                case "Regular":
                    return FontWeights.Regular;
                case "SemiBold":
                    return FontWeights.SemiBold;
                case "Thin":
                    return FontWeights.Thin;
                case "UltraBlack":
                    return FontWeights.UltraBlack;
                case "UltraBold":
                    return FontWeights.UltraBold;
                case "UltraLight":
                    return FontWeights.UltraLight;
            }
            return FontWeights.Normal;
        }

        public static Stretch StringToStretch(string stretch)
        {
            switch (stretch)
            {
                case "None":
                    return Stretch.None;
                case "Fill":
                    return Stretch.Fill;
                case "Uniform":
                    return Stretch.Uniform;
                case "UniformToFill":
                    return Stretch.UniformToFill;
            }
            return Stretch.None;
        }

        public static TimeZoneInfo StringToTimeZoneInfo(string timeZoneInfoString)
        {
            foreach (var v in TimeZoneInfo.GetSystemTimeZones())
            {
                if (v.ToString() == timeZoneInfoString)
                    return v;
            }
            return TimeZoneInfo.Local;
        }
    }
}
