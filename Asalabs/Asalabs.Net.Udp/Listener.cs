﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using Asalabs.Extensions;

namespace Asalabs.Net.Udp
{
    public class Listener : IDisposable
    {
        public event EventHandler<StringArgs> ErrorReceived;
        public event EventHandler<StringArgs> DataReceived;

        public IPAddress Ip { private set; get; }
        public int ListenPort { private set; get; }
        public bool IsListening { private set; get; }
        public bool IsClosing { private set; get; }

        private UdpClient UdpClient { set; get; }

        private IPEndPoint _localEp;

        private volatile bool _done = false;


        public class DataArgs : EventArgs
        {
            public string Data { set; get; }
        }

        private System.Threading.Thread _thread;

        public void StartListening(IPAddress ip, int port)
        {
            try
            {
                if (IsListening) return;

                Ip = ip;
                ListenPort = port;

                UdpClient = new UdpClient {ExclusiveAddressUse = false};

                _localEp = new IPEndPoint(IPAddress.Any, ListenPort);
                UdpClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                UdpClient.ExclusiveAddressUse = false;

                UdpClient.Client.Bind(_localEp);
                UdpClient.JoinMulticastGroup(ip);
                _thread = new System.Threading.Thread(Receive) {IsBackground = true};
                _thread.Start();
                IsClosing = false;
            }
            catch (Exception ex)
            {
                if (ErrorReceived != null)
                    ErrorReceived(this, new StringArgs {Value = ex.Message + "\n" + ex.StackTrace});
            }
        }

        private void Receive()
        {
            try
            {
                while (!_done)
                {
                    var bytes = UdpClient.Receive(ref _localEp);
                    if (bytes.Length > 0)
                    {
                        var data = Encoding.ASCII.GetString(bytes);
                        if (DataReceived != null)
                            DataReceived(this, new StringArgs {Value = data});
                    }
                    IsListening = true;
                    //Console.WriteLine("Received broadcast from {0} :\n {1}\n",groupEP.ToString(),Encoding.ASCII.GetString(bytes, 0, bytes.Length));
                }

            }
            catch (Exception)
            {
                if (!IsClosing)
                    Receive();
            }
        }

        public void StopListening()
        {
            _done = true;
            if (UdpClient != null)
                UdpClient.Close();
            if (_thread != null)
                _thread.Abort();
        }
        public void Dispose()
        {
            IsClosing = true;
            StopListening();
            UdpClient = null;
        }
    }
}
