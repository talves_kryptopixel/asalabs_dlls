﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Asalabs.Net.Udp.Legacy
{
    public class Listener
    {
        public int ListenPort { private set; get; }
        public bool IsListening { private set; get; }

        private UdpClient _listener;
        private IPEndPoint _groupEp;
        private volatile bool _done = false;

        public event EventHandler<DataArgs> DataReceived;

        public class DataArgs : EventArgs
        {
            public string Data { set; get; }
        }

        private System.Threading.Thread _thread;

        public void StartListening(int port)
        {
            try
            {
                if (IsListening) return;

                ListenPort = port;

                _listener = new UdpClient(ListenPort);
                _groupEp = new IPEndPoint(IPAddress.Any, ListenPort);
                _thread = new System.Threading.Thread(Receive);
                _thread.Start();
            }
            catch (Exception ex)
            {
                //System.Windows.MessageBox.Show(ex.StackTrace);
            }
        }

        private void Receive()
        {
            try
            {
                while (!_done)
                {
                    var bytes = _listener.Receive(ref _groupEp);
                    if (bytes.Length > 0)
                    {
                        var data = Encoding.ASCII.GetString(bytes);
                        if (DataReceived != null)
                            DataReceived(this, new DataArgs {Data = data});
                    }
                    IsListening = true;
                    //Console.WriteLine("Received broadcast from {0} :\n {1}\n",groupEP.ToString(),Encoding.ASCII.GetString(bytes, 0, bytes.Length));
                }

            }
            catch (Exception)
            {
                Receive();
            }
            finally
            {
                _listener.Close();
            }
        }

        public void StopListening()
        {
            _done = true;
            if (_listener != null)
                _listener.Close();
            if (_thread != null)
                _thread.Abort();
        }
    }
}
