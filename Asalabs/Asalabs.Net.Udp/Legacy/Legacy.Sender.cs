﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Asalabs.Net.Udp.Legacy
{
    public class Sender
    {
        private Socket Soc { set; get; }
        public int Port { private set; get; }
        public IPAddress Ip { set; get; }

        public Sender(IPAddress ip, int port)
        {
            Ip = ip;
            Port = port;
            Soc = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        }

        public void Send(string text)
        {
            var sendbuf = Encoding.ASCII.GetBytes(text);
            var ep = new IPEndPoint(Ip, Port);
            try
            {
                Soc.SendTo(sendbuf, ep);
            }
            catch (Exception)
            {
            }
        }

        public void Close()
        {
            if (Soc == null) return;
            if (Soc.Connected)
                Soc.Disconnect(false);
            Soc.Close();
        }
    }
}