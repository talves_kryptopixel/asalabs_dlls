﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using Asalabs.Extensions;

namespace Asalabs.Net.Udp
{
    public class Sender : IDisposable
    {
        public event EventHandler<StringArgs> ErrorReceived; 
        private UdpClient UdpClient { set; get; }
        public IPAddress Ip { private set; get; }
        public int Port { private set; get; }
        public short Ttl { private set; get; }
        private IPEndPoint RemoteEp { set; get; }

        public Sender(IPAddress ip, int port, short ttl = 64)
        {
            Ip = ip;
            Port = port;
            Ttl = ttl;
            UdpClient = new UdpClient {ExclusiveAddressUse = false};
            UdpClient.JoinMulticastGroup(Ip);
            UdpClient.Ttl = Ttl;
            RemoteEp = new IPEndPoint(Ip, Port);
        }

        public void Send(string text)
        {
            var sendbuf = Encoding.ASCII.GetBytes(text);
            try
            {
                UdpClient.Send(sendbuf, sendbuf.Length, RemoteEp);
            }
            catch (Exception ex)
            {
                if (ErrorReceived != null)
                    ErrorReceived(this, new StringArgs {Value = ex.Message+"\n"+ex.StackTrace});
            }
        }

        public void Close()
        {
            if (UdpClient == null) return;
            UdpClient.Close();
        }

        public void Dispose()
        {
            Close();
            UdpClient = null;
        }
    }
}
