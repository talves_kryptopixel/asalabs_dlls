﻿using System;
using System.Net;

namespace Asalabs.Net.Automation
{
    public class WindowsServerDhcpReservations : IDisposable
    {
        public WindowsServerDhcpReservations()
        {
        }

        public bool AddReservation(IPAddress ipAddressScope, IPAddress ipAddress, string macAddress)
        {
            var NoError = false;
            Console.Write("\n" + "Started!");
            using (var ps = System.Management.Automation.PowerShell.Create())
            {
                try
                {
                    ps.AddCommand("Add-DhcpServerv4Reservation");
                    ps.AddParameter("IPAddress", ipAddress.ToString());
                    ps.AddParameter("ClientId", macAddress);
                    ps.AddParameter("ScopeId", ipAddressScope.ToString());
                    var psOutput = ps.Invoke();
                    NoError = true;
                    Console.WriteLine("\n" + "Executed!");
                    foreach (var outputItem in psOutput)
                    {
                        if (outputItem != null)
                        {
                            Console.WriteLine("\n" + outputItem.ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("\n" + ex.Message);
                }
                Console.WriteLine("\n" + "Ended!");
            }
            return NoError;
        }
        public bool SetReservation(IPAddress ipAddress, string macAddress)
        {
            var NoError = false;
            Console.Write("\n" + "Started!");
            using (var ps = System.Management.Automation.PowerShell.Create())
            {
                try
                {
                    ps.AddCommand("Set-DhcpServerv4Reservation");
                    ps.AddParameter("IPAddress", ipAddress.ToString());
                    ps.AddParameter("ClientId", macAddress);
                    var psOutput = ps.Invoke();
                    NoError = true;
                    Console.WriteLine("\n" + "Executed!");
                    foreach (var outputItem in psOutput)
                    {
                        if (outputItem != null)
                        {
                            Console.WriteLine("\n" + outputItem.ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("\n" + ex.Message);
                }
                Console.WriteLine("\n" + "Ended!");
            }
            return NoError;
        }
        public bool GetReservations(IPAddress ipAddressScope)
        {
            var NoError = false;
            Console.Write("\n" + "Started!");
            using (var ps = System.Management.Automation.PowerShell.Create())
            {
                try
                {
                    ps.AddCommand("Get-DhcpServerv4Reservation");
                    ps.AddParameter("ScopeId", ipAddressScope.ToString());
                    var psOutput = ps.Invoke();
                    NoError = true;
                    Console.WriteLine("\n" + "Executed!");
                    Console.WriteLine("\n" + "Scope" + "\t" + "IpAddress" + "\t" + "MacAddress");
                    foreach (var outputItem in psOutput)
                    {
                        if (outputItem != null)
                        {
                            Console.WriteLine("\n" + outputItem.Members["ScopeId"].Value + "\t" + outputItem.Members["IPAddress"].Value + "\t" + outputItem.Members["ClientId"].Value);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("\n" + ex.Message);
                }
                Console.WriteLine("\n" + "Ended!");
            }
            return NoError;
        }

        public bool RemoveAllReservations(IPAddress ipAddressScope)
        {
            var NoError = false;
            Console.Write("\n" + "Started!");
            using (var ps = System.Management.Automation.PowerShell.Create())
            {
                try
                {
                    ps.AddCommand("Remove-DhcpServerv4Reservation");
                    ps.AddParameter("ScopeId", ipAddressScope.ToString());
                    var psOutput = ps.Invoke();
                    NoError = true;
                    Console.WriteLine("\n" + "AllRemoved!");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("\n" + ex.Message);
                }
                Console.WriteLine("\n" + "Ended!");
            }
            return NoError;
        }

        public void Dispose()
        {
        }

    }
}
