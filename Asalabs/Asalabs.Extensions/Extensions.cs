﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Asalabs.Extensions
{
    public static class Extentions
    {
        public static int ToInt(this string text)
        {
            var i = 0;
            var result = Regex.Replace(text, @"[^\d]", "");
            if (result.Length > 0)
                i = Int32.Parse(result);
            return i;
        }
        public static Double ToDouble(this string text)
        {
            Double f = 0;
            var result = Regex.Replace(text, @"[^\d]^.", "");
            if (result.Length > 0)
                Double.TryParse(result, out f);
            return f;
        }

        public static string ToMinutesAndSecondsString(this TimeSpan ts)
        {
            return string.Format("{0:00}:{1:00}", ts.Minutes + (int)ts.TotalHours * 60, ts.Seconds);

        }
    }

    public class BoolArgs : EventArgs
    {
        public bool Value { set; get; }
    }

    public class IntArgs : EventArgs
    {
        public int Value { set; get; }
    }

    public class DoubleArgs : EventArgs
    {
        public double Value { set; get; }
    }

    public class StringArgs : EventArgs
    {
        public string Value { set; get; }
    }

    public static class StringFormat
    {
    }
}
