﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Management;
using System.Windows;
using Asalabs.Converters;
using Asalabs.ErrorReport;

namespace Asalabs.Activation
{
    public static class Security
    {
        public static string Key = "KfeFBtgon69rvYeV5TtCtZ8KOpKnnSBZXdcRZLndQto=";
        public static string IV = "gEZ2FYXjBwdCsyY9Ftfgrw==";
    }

    public static class Activation
    {
        //private static string EncryptionKey = "dsf76sdf9sdfsd7cvfb7cbvdfghuh7sdf7";

        public static string GetSystemId()
        {
            return BiosSerial;
        }

        private static string Generate(string appId, string machineId, string hash)
        {
            char[] _code = new char[41];
            string _Hash = hash.ToUpper();
            string _AppID = appId.ToUpper();
            string _MachineId = machineId.ToUpper();
            int machineChecksum = 0;
            int ax = 0, x = 0, y = 0, z = 0, index = 0;
            foreach (char c in _MachineId)
                machineChecksum += (int) c;
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    if (x >= (_MachineId.Length - 1))
                        x = 0;
                    if (y >= (_Hash.Length - 1))
                        y = 0;
                    if (z >= (_AppID.Length - 1))
                        z = 0;

                    ax = ((int) (_MachineId[x++]) + (int) (_Hash[y++])) + (int) (_AppID[z++]) + machineChecksum;
                    while (ax > 90)
                        ax -= 42;
                    if (ax > 57 && ax < 65)
                        ax = (65 - 57) + ax;
                    _code[index++] = (char) ax;
                }
                if (i < 5)
                    _code[index++] = '-';
            }
            return new string(_code);
        }

        private static string Generate(string appId, string Hash)
        {
            return Generate(appId, BiosSerial, Hash);
        }

        private static string CpuId
        {
            get
            {
                var cpuInfo = String.Empty;
                //create an instance of the Managemnet class with the
                //Win32_Processor class
                var mgmt = new ManagementClass("Win32_Processor");
                //create a ManagementObjectCollection to loop through
                var objCol = mgmt.GetInstances();
                //start our loop for all processors found
                foreach (var o in objCol)
                {
                    var obj = (ManagementObject) o;
                    if (cpuInfo == String.Empty)
                    {
                        // only return cpuInfo from first CPU
                        cpuInfo = obj.Properties["ProcessorId"].Value.ToString().ToUpper();
                    }
                }
                return cpuInfo;
            }
        }

        private static string BiosSerial
        {
            get
            {
                var biosInfo = String.Empty;
                //create an instance of the Managemnet class with the
                //Win32_Processor class
                var mgmt = new ManagementClass("Win32_BIOS");
                //create a ManagementObjectCollection to loop through
                var objCol = mgmt.GetInstances();
                //start our loop for all processors found
                foreach (var o in objCol)
                {
                    var obj = (ManagementObject) o;
                    if (biosInfo == String.Empty)
                    {
                        biosInfo = obj.Properties["SerialNumber"].Value.ToString().ToUpper();
                    }
                }
                return biosInfo;
            }
        }

        public static bool Validate(string appId, string activationCode)
        {
            string _code = "CalculatedCode";
            string code = "SettingsCode";
            string hash = "www.asalabs.com";

            try
            {
                byte[] key = Converter.DecodeFromString(Security.Key);
                byte[] IV = Converter.DecodeFromString(Security.IV);
                if (string.IsNullOrEmpty(activationCode))
                    return false;
                byte[] data = Converter.DecodeFromString(activationCode);
                if (data != null)
                    code = AesCrypt.DecryptStringFromBytes_Aes(data, key, IV);
                _code = Generate(appId, hash);
                if (code == _code)
                    return true;
            }
            catch (Exception ex)
            {
                Errors.Show("Validate", ex, Errors.ErrorType.Warning);
            }
            return false;
        }
    }


    public static class AesCrypt
    {

        public static AesManaged Aes = new AesManaged();

        public static byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");
            byte[] encrypted;
            // Create an AesManaged object 
            // with the specified key and IV. 
            using (var aesAlg = new AesManaged())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption. 
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            // Return the encrypted bytes from the memory stream. 
            return encrypted;
        }

        public static string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            // Declare the string used to hold 
            // the decrypted text. 
            string plaintext = null;

            // Create an AesManaged object 
            // with the specified key and IV. 
            using (var aesAlg = new AesManaged())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption. 
                using (var msDecrypt = new MemoryStream(cipherText))
                {
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream 
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            return plaintext;
        }

        public class Rc4Crypt
        {
        /*
        public static string RC4Encrypt(string Data, string Key)
        {
            long len = 255;
            //Create an aux Copy
            byte[] EncryptHash = new byte[len];

            // Used to populate m_nBox
            long index2 = 0;

            // Create two different encoding
            Encoding ascii = Encoding.ASCII;
            Encoding unicode = Encoding.Unicode;

            //
            // Perform the conversion of the encryption key from unicode to ansi
            //
            byte[] asciiBytes = Encoding.Convert(unicode, ascii, unicode.GetBytes(Key));

            //
            // Convert the new byte[] into a char[] and then to string
            //

            char[] asciiChars = new char[ascii.GetCharCount(asciiBytes, 0, asciiBytes.Length)];
            ascii.GetChars(asciiBytes, 0, asciiBytes.Length, asciiChars, 0);
            string EncryptionKeyAscii = new string(asciiChars);

            //
            // Populate m_nBox
            //
            long KeyLen = Key.Length;

            //
            // First Loop
            //
            for (long count = 0; count < len; count++)
            {
                EncryptHash[count] = (byte)count;
            }

            //
            // Second Loop
            //
            for (long count = 0; count < len; count++)
            {
                index2 = (index2 + EncryptHash[count] + asciiChars[count % KeyLen]) % len;
                byte temp = EncryptHash[count];
                EncryptHash[count] = EncryptHash[index2];
                EncryptHash[index2] = temp;
            }

            try
            {
                Encoding encoding = Encoding.Default;
                long i = 0;
                long j = 0;

                // Convert to byte array
                byte[] UnencryptedData = encoding.GetBytes(Data);

                byte[] EncryptHashCopy = new byte[UnencryptedData.Length];
                EncryptHash.CopyTo(EncryptHashCopy, 0);

                // Output byte array
                byte[] EncryptedData = new byte[UnencryptedData.Length];

                //	Len of Chipher
                long ChipherLen = UnencryptedData.Length + 1;

                //
                // Run Alghoritm
                //
                for (long offset = 0; offset < UnencryptedData.Length; offset++)
                {
                    i = (i + 1) % len;
                    j = (j + EncryptHashCopy[i]) % len;
                    byte temp = EncryptHashCopy[i];
                    EncryptHashCopy[i] = EncryptHashCopy[j];
                    EncryptHashCopy[j] = temp;
                    byte a = UnencryptedData[offset];
                    byte b = EncryptHashCopy[(EncryptHashCopy[i] + EncryptHashCopy[j]) % len];
                    EncryptedData[offset] = (byte)((int)a ^ (int)b);
                }

                //
                // Put result into output string ( CryptedText )
                //
                char[] outarrchar = new char[encoding.GetCharCount(EncryptedData, 0, EncryptedData.Length)];
                encoding.GetChars(EncryptedData, 0, EncryptedData.Length, outarrchar, 0);
                return new string(outarrchar);
            }
            catch (Exception ex)
            {
                Errors.Show("Encrypt", ex, Errors.ErrorType.Exception);
            }
            return null;
        }


        public class RC4Engine
        {
            #region Costructor

            public RC4Engine()
            {
            }

            #endregion

            #region Public Method

            /// <summary>
            /// Encript InClear Text using RC4 method using EncriptionKey
            /// Put the result into CryptedText 
            /// </summary>
            /// <returns>true if success, else false</returns>
            public bool Encrypt()
            {
                //
                // toRet is used to store function retcode
                //
                bool toRet = true;

                try
                {
                    //
                    // indexes used below
                    //
                    long i = 0;
                    long j = 0;

                    //
                    // Put input string in temporary byte array
                    //
                    Encoding enc_default = Encoding.Default;
                    byte[] input = enc_default.GetBytes(this.m_sInClearText);

                    // 
                    // Output byte array
                    //
                    byte[] output = new byte[input.Length];

                    //
                    // Local copy of m_nBoxLen
                    //
                    byte[] n_LocBox = new byte[m_nBoxLen];
                    this.m_nBox.CopyTo(n_LocBox, 0);

                    //
                    //	Len of Chipher
                    //
                    long ChipherLen = input.Length + 1;

                    //
                    // Run Alghoritm
                    //
                    for (long offset = 0; offset < input.Length; offset++)
                    {
                        i = (i + 1) % m_nBoxLen;
                        j = (j + n_LocBox[i]) % m_nBoxLen;
                        byte temp = n_LocBox[i];
                        n_LocBox[i] = n_LocBox[j];
                        n_LocBox[j] = temp;
                        byte a = input[offset];
                        byte b = n_LocBox[(n_LocBox[i] + n_LocBox[j]) % m_nBoxLen];
                        output[offset] = (byte)((int)a ^ (int)b);
                    }

                    //
                    // Put result into output string ( CryptedText )
                    //
                    char[] outarrchar = new char[enc_default.GetCharCount(output, 0, output.Length)];
                    enc_default.GetChars(output, 0, output.Length, outarrchar, 0);
                    this.m_sCryptedText = new string(outarrchar);
                }
                catch
                {
                    //
                    // error occured - set retcode to false.
                    // 
                    toRet = false;
                }

                //
                // return retcode
                //
                return (toRet);

            }

            /// <summary>
            /// Decript CryptedText into InClearText using EncriptionKey
            /// </summary>
            /// <returns>true if success else false</returns>
            public bool Decrypt()
            {
                //
                // toRet is used to store function retcode
                //
                bool toRet = true;

                try
                {
                    this.m_sInClearText = this.m_sCryptedText;
                    m_sCryptedText = "";
                    if (toRet = Encrypt())
                    {
                        m_sInClearText = m_sCryptedText;
                    }

                }
                catch
                {
                    //
                    // error occured - set retcode to false.
                    // 
                    toRet = false;
                }

                //
                // return retcode
                //
                return toRet;
            }

            #endregion

            #region Prop definitions
            /// <summary>
            /// Get or set Encryption Key
            /// </summary>
            public string EncryptionKey
            {
                get
                {
                    return (this.m_sEncryptionKey);
                }
                set
                {
                    //
                    // assign value only if it is a new value
                    //
                    if (this.m_sEncryptionKey != value)
                    {
                        this.m_sEncryptionKey = value;

                        //
                        // Used to populate m_nBox
                        //
                        long index2 = 0;

                        //
                        // Create two different encoding 
                        //
                        Encoding ascii = Encoding.ASCII;
                        Encoding unicode = Encoding.Unicode;

                        //
                        // Perform the conversion of the encryption key from unicode to ansi
                        //
                        byte[] asciiBytes = Encoding.Convert(unicode, ascii, unicode.GetBytes(this.m_sEncryptionKey));

                        //
                        // Convert the new byte[] into a char[] and then to string
                        //

                        char[] asciiChars = new char[ascii.GetCharCount(asciiBytes, 0, asciiBytes.Length)];
                        ascii.GetChars(asciiBytes, 0, asciiBytes.Length, asciiChars, 0);
                        this.m_sEncryptionKeyAscii = new string(asciiChars);

                        //
                        // Populate m_nBox
                        //
                        long KeyLen = m_sEncryptionKey.Length;

                        //
                        // First Loop
                        //
                        for (long count = 0; count < m_nBoxLen; count++)
                        {
                            this.m_nBox[count] = (byte)count;
                        }

                        //
                        // Second Loop
                        //
                        for (long count = 0; count < m_nBoxLen; count++)
                        {
                            index2 = (index2 + m_nBox[count] + asciiChars[count % KeyLen]) % m_nBoxLen;
                            byte temp = m_nBox[count];
                            m_nBox[count] = m_nBox[index2];
                            m_nBox[index2] = temp;
                        }

                    }
                }
            }

            public string InClearText
            {
                get
                {
                    return (this.m_sInClearText);
                }
                set
                {
                    //
                    // assign value only if it is a new value
                    //
                    if (this.m_sInClearText != value)
                    {
                        this.m_sInClearText = value;
                    }
                }
            }

            public string CryptedText
            {
                get
                {
                    return (this.m_sCryptedText);
                }
                set
                {
                    //
                    // assign value only if it is a new value
                    //
                    if (this.m_sCryptedText != value)
                    {
                        this.m_sCryptedText = value;
                    }
                }
            }
            #endregion

            #region Private Fields

            //
            // Encryption Key  - Unicode & Ascii version
            //
            private string m_sEncryptionKey = "";
            private string m_sEncryptionKeyAscii = "";
            //
            // It is related to Encryption Key
            //
            protected byte[] m_nBox = new byte[m_nBoxLen];
            //
            // Len of nBox
            //
            static public long m_nBoxLen = 255;

            //
            // In Clear Text
            //
            private string m_sInClearText = "";
            private string m_sCryptedText = "";

            #endregion

        }
    }*/
        }
    }
}
