﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;


namespace Asalabs.Activation
{
    /// <summary>
    /// Interaction logic for ActivationPage.xaml
    /// </summary>
    public partial class ActivationWindow : Window
    {
        public class ActivationArgs : EventArgs
        {
            public string ActivationCode;
        }

        public event EventHandler<ActivationArgs> ActivationKeyChanged;
        
        public ActivationWindow(string appName,string appId)
        {
            InitializeComponent();
            this.Topmost = true;
            lbAppName.Content = appName;
            tbAppID.Text = appId;
            tbID.Text = Activation.GetSystemId();
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                if (ActivationScreen.Visibility == System.Windows.Visibility.Visible)
                    btActivate_Click(this, new RoutedEventArgs());

            base.OnKeyDown(e);
        }

        private void btActivate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                byte[] crypted = AesCrypt.EncryptStringToBytes_Aes(tbActivationCode.Text, System.Convert.FromBase64String(Asalabs.Activation.Security.Key), System.Convert.FromBase64String(Asalabs.Activation.Security.IV));
                if (ActivationKeyChanged != null)
                    ActivationKeyChanged(this, new ActivationArgs {ActivationCode = System.Convert.ToBase64String(crypted)});
            }
            catch (Exception ex)
            {
                Asalabs.ErrorReport.Errors.Show("Encryptation Failed!",ex,ErrorReport.Errors.ErrorType.Warning);
            }
        }

        private void tbActivationCode_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextboxToUpper((TextBox)sender);
        }

        private void TextboxToUpper(TextBox sender)
        {
            int idx = sender.CaretIndex;
            if (sender.Text != sender.Text.ToUpper())
            {
                sender.Text = sender.Text.ToUpper();
                sender.CaretIndex = idx;
            }
        }

        private void btClose_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }
    }
}
