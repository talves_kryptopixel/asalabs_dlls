﻿namespace Asalabs.Timers
{
    public class ImpulseStateHold
    {
        System.Timers.Timer t = new System.Timers.Timer(100);

        public bool State { private set; get; }
        public void Hold(int miliseconds)
        {
            State = true;
            _counter = miliseconds;
        }
        public void Reset()
        {
            _counter = 0;
            State = false;
        }
        int _counter = 0;
        public ImpulseStateHold()
        {
            t.AutoReset = true;
            t.Elapsed += t_Elapsed;
            t.Enabled = true;
            t.Start();
        }

        void t_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if(_counter<=0)
            {   
                State = false;
                return;
            }
            _counter-=100;
        }
    }
}
