﻿using System;

namespace Asalabs.Timers
{
    public class TimeCounter : IDisposable
    {
        readonly System.Threading.Thread _thread;
        System.Timers.Timer _t;
        public event EventHandler Tick;
        public event EventHandler PersistentTick;
        public event EventHandler Stopping;
        public event EventHandler Starting;
        public bool Counting { get { return _counting; } }
        bool _disposing = false;
        bool _counting = false;
        bool _restarting = true;
        public void Start()
        {
            if (!_counting)
            {
                _counting = true;
                _restarting = true;
                if (Starting != null)
                    Starting(this, new EventArgs());
            }
        }
        public void Stop()
        {
            if (_counting)
            {
                _counting = false;
                _restarting = true;
                if (Stopping != null)
                    Stopping(this, new EventArgs());
            }
        }

        public TimeCounter()
        {
            _thread = new System.Threading.Thread(Inicialize) {IsBackground = true};
            _thread.Start();
        }

        public void Dispose()
        {
            _disposing = true;
            _thread.Abort();
        }

        public void Inicialize()
        {
            _t = new System.Timers.Timer(100) {AutoReset = true};
            _t.Elapsed += t_Elapsed;
            _t.Enabled = true;
            _t.Start();
        }

        DateTime _lastTime = DateTime.Now;
        void t_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (_lastTime.Second != DateTime.Now.Second)
            {
                _lastTime = DateTime.Now;
                if (_counting)
                {
                    if (!_restarting)
                        if (Tick != null)
                            Tick(this, new EventArgs());
                    _restarting = false;
                }
                if (PersistentTick != null)
                    PersistentTick(this, new EventArgs());
            }
            if (_disposing)
            {
                _t.Enabled = false;
                _t.Stop();
                _t.Elapsed -= t_Elapsed;
                _t.Close();
                _t.Dispose();
            }
        }
    }
}
