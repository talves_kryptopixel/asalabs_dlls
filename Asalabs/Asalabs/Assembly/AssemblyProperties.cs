﻿namespace Asalabs.Assembly
{

    public static class AssemblyProperties
    {
        #region Assembly Attribute Accessors

        public static string AssemblyTitle
        {
            get
            {
                var attributes = System.Reflection.Assembly.GetCallingAssembly().GetCustomAttributes(typeof(System.Reflection.AssemblyTitleAttribute), false);
                if (attributes.Length <= 0)
                    return
                        System.IO.Path.GetFileNameWithoutExtension(
                            System.Reflection.Assembly.GetCallingAssembly().CodeBase);
                var titleAttribute = (System.Reflection.AssemblyTitleAttribute)attributes[0];
                return titleAttribute.Title != "" ? titleAttribute.Title : System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetCallingAssembly().CodeBase);
            }
        }

        public static string AssemblyVersion
        {
            get
            {
                return System.Reflection.Assembly.GetCallingAssembly().GetName().Version.ToString();
            }
        }

        public static string AssemblyDescription
        {
            get
            {
                var attributes = System.Reflection.Assembly.GetCallingAssembly().GetCustomAttributes(typeof(System.Reflection.AssemblyDescriptionAttribute), false);
                return attributes.Length == 0 ? "" : ((System.Reflection.AssemblyDescriptionAttribute)attributes[0]).Description;
            }
        }

        public static string AssemblyProduct
        {
            get
            {
                var attributes = System.Reflection.Assembly.GetCallingAssembly().GetCustomAttributes(typeof(System.Reflection.AssemblyProductAttribute), false);
                return attributes.Length == 0 ? "" : ((System.Reflection.AssemblyProductAttribute)attributes[0]).Product;
            }
        }

        public static string AssemblyCopyright
        {
            get
            {
                var attributes = System.Reflection.Assembly.GetCallingAssembly().GetCustomAttributes(typeof(System.Reflection.AssemblyCopyrightAttribute), false);
                return attributes.Length == 0 ? "" : ((System.Reflection.AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }

        public static string AssemblyCompany
        {
            get
            {
                var attributes = System.Reflection.Assembly.GetCallingAssembly().GetCustomAttributes(typeof(System.Reflection.AssemblyCompanyAttribute), false);
                return attributes.Length == 0 ? "" : ((System.Reflection.AssemblyCompanyAttribute)attributes[0]).Company;
            }
        }
        #endregion
    }
}
