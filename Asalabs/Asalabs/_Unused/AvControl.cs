﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AvControl
{
    public enum ActionType { Inquire, Request, Answer, Welcome, Status, Error };
    public enum Transport { Null, Restart, Play, Stop, Pause, Load };
    public enum Repeat { Null, Enable, Disable }
    public enum Audio { Null, Mute, UnMute }
    public enum Media { Null, Unloaded, Loaded, Failed, Opened };
    public enum Sys { Null, Shutdown, Restart, Startup };
    public enum Evac { Null, Disable, Enable }
    public enum Connection { Null, Disconnected, Connected };
    public enum Operation { Null, Standby, Operating };
    public enum SMotor { Null, Start, Stop };
    public enum RemIO { Null, Enable, Disable };
    public enum Power { Null, On, Off, Cooling, Reset };
    public enum VideoInput { Null, AV, YC, YUV, VGA, RGB, RGB1, RGB2, DVI, HDMI, HDMI1, HDMI2, DP };

    public enum CommandTypes { Null, Transport, Repeat, Audio, Media, Sys, Evac, Connection, Operation, SMotor, RemIO, Power };

    public class StatusArgs : EventArgs
    {
        public Transport TransportStatus = Transport.Null;
        public Media MediaStatus = Media.Null;
        public Repeat RepeatStatus = Repeat.Null;
        public Audio AudioStatus = Audio.Null;
        public Sys SystemStatus = Sys.Null;
        public Evac EvacStatus = Evac.Null;
        public Connection ConnectionStatus = Connection.Null;
        public Operation OperationStatus = Operation.Null;
        public Power PowerStatus = Power.Null;        
    }


}



























    /*
    class SocketMessage
    {
        public static string Create(Action act, Transport arg)
        {
            string type = typeof(Transport).ToString();
            return "#" + act.ToString() + " " + type + " " + arg.ToString();
        }

        public static string Create(Action act, System arg)
        {
            string type = typeof(Transport).ToString();
            return "#" + act.ToString() + " " + type + " " + arg.ToString();
        }

        public static string Create(Action act, Audio arg)
        {
            string type = typeof(Transport).ToString();
            return "#" + act.ToString() + " " + type + " " + arg.ToString();
        }

        public static string Create(Action act, Repeat arg)
        {
            string type = typeof(Transport).ToString();
            return "#" + act.ToString() + " " + type + " " + arg.ToString();
        }

        public static string Create(Action act, Evac arg)
        {
            string type = typeof(Transport).ToString();
            return "#" + act.ToString() + " " + type + " " + arg.ToString();
        }

        public static string Create(Action act, Operation arg)
        {
            string type = typeof(Transport).ToString();
            return "#" + act.ToString() + " " + type + " " + arg.ToString();
        }

        public static string Create(Action act, Media arg)
        {
            string type = typeof(Transport).ToString();
            return "#" + act.ToString() + " " + type + " " + arg.ToString();
        }

        public static string Create(Action act, Connection arg)
        {
            string type = typeof(Transport).ToString();
            return "#" + act.ToString() + " " + type + " " + arg.ToString();
        }
    }
    */