﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Asalabs.ZoomControl
{
    /// <summary>
    /// Interaction logic for ZoomControl.xaml
    /// </summary>
    public partial class SliderZoomControl : UserControl
    {
        public class ZoomArgs :EventArgs
        {
            public int Value;
        }
        public event EventHandler<ZoomArgs> ZoomChanged;
        public ScaleTransform ScaleT { set; get; }
        public int Minimum { set { sldZoom.Minimum = value; } get { return (int)sldZoom.Minimum; } }
        public int Maximum { set { sldZoom.Maximum = value; } get { return (int)sldZoom.Maximum; } }
        int scaleValue = 100;
        public int ScaleValue
        {
            set
            {
                if (scaleValue != value)
                {
                    scaleValue = value;
                    UpdateValue(scaleValue);
                }
            }
            get { return scaleValue; }
        }

        public SliderZoomControl()
        {
            InitializeComponent();
        }

        private void sldZoom_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ScaleValue = (int)e.NewValue;
        }

        public void InicializeZoom(int startZoom)
        {
            sldZoom.ValueChanged += new RoutedPropertyChangedEventHandler<double>(sldZoom_ValueChanged);
            tbZoom.PreviewGotKeyboardFocus += new KeyboardFocusChangedEventHandler(tbZoom_PreviewGotKeyboardFocus);
            tbZoom.LostFocus += new RoutedEventHandler(tbZoom_LostFocus);
            tbZoom.PreviewKeyDown += new KeyEventHandler(tbZoom_PreviewKeyDown);
            tbZoom.PreviewTextInput += new TextCompositionEventHandler(tbZoom_PreviewTextInput);
            ScaleValue = startZoom;
        }

        void tbZoom_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(tbZoom.Text) && !tbZoom.Text.Contains('%'))
            {
                int val = int.Parse(tbZoom.Text);
                if (val < Minimum)
                    val = Minimum;
                if (val > Maximum)
                    val = Maximum;
                ScaleValue = val;
                UpdateValue(scaleValue);
            }
            else
                UpdateValue(scaleValue);
        }

        void tbZoom_PreviewGotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            tbZoom.Text = scaleValue.ToString();
            tbZoom.CaretIndex = tbZoom.Text.Length;
        }

        void tbZoom_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (ValidatorArgs.Contains(e.Text))
                e.Handled = false;
            else
                e.Handled = true;
        }

        private const string ValidatorArgs = "0123456789";

        void tbZoom_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (!String.IsNullOrEmpty(tbZoom.Text))
                {
                    int val = int.Parse(tbZoom.Text);
                    if (val < Minimum)
                        val = Minimum;
                    if (val > Maximum)
                        val = Maximum;
                    ScaleValue = val;
                }
            }
            if (e.Key == Key.Escape)
                UpdateValue(scaleValue);
        }

        void UpdateValue(int value)
        {
            ScaleT.ScaleX = 0.01 * value;
            ScaleT.ScaleY = 0.01 * value;
            sldZoom.Value = value;
            tbZoom.Text = value + "%";
            if (ZoomChanged != null)
                ZoomChanged(this, new ZoomArgs {Value = value});
        }
    }
}
