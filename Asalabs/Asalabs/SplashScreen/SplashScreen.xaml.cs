﻿using System;
using System.Diagnostics;
using System.Timers;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace Asalabs.SplashScreen
{
    /// <summary>
    /// Interaction logic for SplashScreen.xaml
    /// </summary>
    public partial class SplashScreen : Window
    {
        private Timer _timer;
        public bool AutoClose { set; get; }
        public string AssemblyProduct { set; get; }
        public string AssemblyVersion { set; get; }
        public bool IsActivated { set; get; }
        public string LicenseType { set; get; }
        //
        public Visibility MadeInPortugal
        {
            set { lbMadeInPortugal.Visibility = value; }
            get { return lbMadeInPortugal.Visibility; }
        }

        public SplashScreen()
        {
            InitializeComponent();
            this.Topmost = true;
        }

        protected override void OnActivated(EventArgs e)
        {
            this.LbProductName.Content = String.Format("{0}", AssemblyProduct);
            this.LbProductVersion.Content = String.Format("Version : {0}", AssemblyVersion);
            if (IsActivated)
            {
                LbType.Foreground = Brushes.White;
                LbType.Content = LicenseType;
            }
            if (AutoClose)
            {
                _timer = new Timer(3000) {AutoReset = false};
                _timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
                _timer.Enabled = true;
                _timer.Start();
            }
            else
                PreviewMouseDown += new MouseButtonEventHandler(SplashScreen_PreviewMouseDown);
            base.OnActivated(e);
        }

        void SplashScreen_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _timer.Enabled = false;
            _timer.Stop();
            _timer.Close();
            _timer.Dispose();
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
            {
                DialogResult = true;
            }));
        }

        private void Asalabs_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            Process.Start("http://www.asalabs.com");
            e.Handled = true;
        }
    }
}
