﻿using System;
using System.Collections.Generic;

namespace Asalabs.UndoRedo
{
    public class UndoRedo
    {
        List<UndoRedoElement> UndoRedoList = new List<UndoRedoElement>();
        int index = -1;

        public event EventHandler UndoStateChanged;
        public event EventHandler RedoStateChanged;

        bool undoEnable = false;
        public bool UndoEnable
        {
            set { undoEnable = value; if (UndoStateChanged != null) UndoStateChanged(this, new EventArgs()); }
            get { return undoEnable; }
        }

        bool redoEnable = false;
        public bool RedoEnable
        {
            set { redoEnable = value; if (RedoStateChanged != null) RedoStateChanged(this, new EventArgs()); }
            get { return redoEnable; }
        }
        public enum UndoRedoActionTypes { Create, Delete, Group, Ungroup , ValueChange}
        public class UndoRedoAction
        {
            public object Element { set; get; }
            public object Property { set; get; }
            public object OldValue { set; get; }
            public object NewValue { set; get; }
        }

        public class UndoRedoElement
        {
            public UndoRedoActionTypes ActionType { set; get; }
            List<UndoRedoAction> undoRedoActions = new List<UndoRedoAction>();
            public List<UndoRedoAction> UndoRedoActions { set { undoRedoActions = value; } get { return undoRedoActions; } }
        }

        public UndoRedoElement Undo()
        {
            UndoRedoElement ure = UndoRedoList[index];
            index--;
            Update();
            return ure;
        }

        public UndoRedoElement Redo()
        {
            index++;
            UndoRedoElement ure = UndoRedoList[index];
            Update();
            return ure;
        }

        void Update()
        {
            if (index > -1 && index <= UndoRedoList.Count - 1) UndoEnable = true; else UndoEnable = false;
            if (index >= -1 && index < UndoRedoList.Count - 1) RedoEnable = true; else RedoEnable = false;
        }

        public void AddElement(UndoRedoElement element)
        { 
            while (index < UndoRedoList.Count - 1)
                if (index > -1)
                    UndoRedoList.RemoveAt(index);
                else
                {
                    UndoRedoList.Clear();
                    break;
                }

            UndoRedoList.Add(element);
            index = UndoRedoList.Count - 1;
            Update();
        }

        public UndoRedo()
        {
        }

        public void Clear()
        {
            UndoRedoList.Clear();
            if (UndoStateChanged != null)
                UndoStateChanged(this, new EventArgs());
        }


    }
}
