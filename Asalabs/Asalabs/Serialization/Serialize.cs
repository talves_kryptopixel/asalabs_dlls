﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Xml.Serialization;

namespace Asalabs.Serialization
{
    public static class Base64Serialization
    {
        public static object Deserialize(string s)
        {
            // We need to know the exact length of the string - Base64 can sometimes pad us by a byte or two
            var p = s.IndexOf(':');
            var length = Convert.ToInt32(s.Substring(0, p));
            // Extract data from the base 64 string!
            var memorydata = Convert.FromBase64String(s.Substring(p + 1));
            var rs = new MemoryStream(memorydata, 0, length);
            var sf = new BinaryFormatter();
            var o = sf.Deserialize(rs);
            return o;
        }

        public static string Serialize(object o)
        {
            // Serialize to a base 64 string
            var ws = new MemoryStream();
            var sf = new BinaryFormatter();
            sf.Serialize(ws, o);
            var bytes = ws.GetBuffer();
            var encodedData = bytes.Length + ":" +
                              Convert.ToBase64String(bytes, 0, bytes.Length, Base64FormattingOptions.None);
            return encodedData;
        }
    }

    public static class XmlSerialization
    {
        public static XmlDocument Serialize(object o)
        {
            var xDoc = new XmlDocument();
            try
            {
                var x = new XmlSerializer(o.GetType());
                using (var sww = new StringWriter())
                {
                    var xw = XmlWriter.Create(sww);
                    x.Serialize(xw, o);
                    xDoc.InnerXml = sww.ToString();
                }
                return xDoc;
            }
            catch (Exception ex)
            {
                return xDoc;
            }
        }

        public static object Deserialize(XmlDocument xDoc, Type type)
        {
            try
            {
                var x = new XmlSerializer(type);
                using (var sww = new StringReader(xDoc.InnerXml))
                {
                    var xw = XmlReader.Create(sww);
                    return x.Deserialize(xw);
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}