﻿using System;
using System.Security.Cryptography;

namespace Asalabs.Security
{
    public class RsaCrypt
    {
        public static RSAParameters PublicKeyInfo { set; get; }
        public static RSAParameters PrivateKeyInfo { set; get; }

        public RsaCrypt()
        {
            /* Initialize RSA and Get Private and Public KeyInfo in paramters */
            var rsa = new RSACryptoServiceProvider();
            rsa.KeySize = 1024;

            /* Generating Info that Contain Public Key Info and Private Key Info*/
            PublicKeyInfo = rsa.ExportParameters(false);
            PrivateKeyInfo = rsa.ExportParameters(true);
        }

        /* Encrypt Data using Public Key info */
        public static byte[] Encrypt(byte[] dataToEncrypt)
        {
            var rsa = new RSACryptoServiceProvider();
            // Encrypt Data using Public Key Info
            rsa.ImportParameters(PublicKeyInfo);
            byte[] encryptedByte = rsa.Encrypt(dataToEncrypt, false);
            rsa.Dispose();
            return encryptedByte;
        }

        /* Decrypt Encrypted Data using Private KeyInfo*/
        public static byte[] Decrypt(byte[] encryptedByte)
        {
            var rsa = new RSACryptoServiceProvider();
            rsa.ImportParameters(PrivateKeyInfo);
            // Decrypt Data
            byte[] decryptedByte = rsa.Decrypt(encryptedByte, false);
            rsa.Dispose();
            return decryptedByte;
        }

        public static string EncodeToString(byte[] data)
        {
            return Convert.ToBase64String(data);
        }

        public static byte[] DecodeFromString(string data)
        {
            return Convert.FromBase64String(data);
        }
    }

}
