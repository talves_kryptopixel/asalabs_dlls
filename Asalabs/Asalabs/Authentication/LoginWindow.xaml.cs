﻿using System;
using System.Diagnostics;
using System.Timers;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using System.Text;

namespace Asalabs.Authentication
{
    /// <summary>
    /// Interaction logic for SplashScreen.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public string AssemblyProduct { set; get; }
        //
        public Visibility MadeInPortugal
        {
            set { lbMadeInPortugal.Visibility = value; }
            get { return lbMadeInPortugal.Visibility; }
        }

        public string _user = "sadkjfbasldkfjabsdlkfjbasdkfjabsdlk.fjbalfjhvbç-ckolabdsçlkjcbasd";

        public string User
        {
            set { _user = value; }
        }


        public string _pass = ".lksdnfvçarfghp945jyçg-dngp93wq4hgçºaoihgpae93845h+3hgaqrgd.klfuigh";

        public string Password
        {
            set { _pass = value; }
        }

        public LoginWindow()
        {
            InitializeComponent();
            this.Topmost = true;
        }

        protected override void OnActivated(EventArgs e)
        {
            this.LbProductName.Content = String.Format("{0}", AssemblyProduct);
            base.OnActivated(e);
        }

        private void Asalabs_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            Process.Start("http://www.asalabs.com");
        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void btLogin_Click(object sender, RoutedEventArgs e)
        {
            if (tbUser.Text == _user && tbPass.Password == _pass)
                DialogResult = true;
            else
                LbAuthenticationFail.Visibility = Visibility.Visible;

        }
        
        private void tbUser_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            LbAuthenticationFail.Visibility = Visibility.Hidden;
        }

        private void tbPass_PasswordChanged(object sender, RoutedEventArgs e)
        {
            LbAuthenticationFail.Visibility = Visibility.Hidden;
        }
    }
}
