﻿using System.IO;
using Asalabs.Properties;

namespace Asalabs.Helper
{
    public class Helper
    {
        public static string GetImagePath(string selectedPath)
        {
            var ofd = new System.Windows.Forms.OpenFileDialog
            {
                RestoreDirectory = true,
                Filter = Resources.AllImagesFilter,
                Multiselect = false
            };
            if (!string.IsNullOrEmpty(selectedPath))
            {
                ofd.InitialDirectory = Path.GetDirectoryName(selectedPath);
                ofd.FileName = Path.GetFileName(selectedPath);
            }
            return ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK ? ofd.FileName : null;
        }

        public static string GetFolder(string selectedPath)
        {
            var fbd = new System.Windows.Forms.FolderBrowserDialog
            {
                ShowNewFolderButton = true
            };
            if (!string.IsNullOrEmpty(selectedPath))
                fbd.SelectedPath = Path.GetFullPath(selectedPath);
            return fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK ? fbd.SelectedPath : null;
        }
    }

}
