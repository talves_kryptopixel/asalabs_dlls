﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Player
{
    public partial class UsbDetect : Form
    {
        public UsbDetect()
        {
            InitializeComponent();
        }


        private const int DeviceInserted = 0x8000;
        private const int DeviceRemoved = 0x8004;

        protected override void WndProc(ref Message m)
        {
            if (m.WParam.ToInt32() == DeviceInserted)
            {
                MessageBox.Show("Device Inserted !");
            }
            else if (m.WParam.ToInt32() == DeviceRemoved)
            {
                MessageBox.Show("Device Removed !");
            }
            base.WndProc(ref m);
        }
    }
}
