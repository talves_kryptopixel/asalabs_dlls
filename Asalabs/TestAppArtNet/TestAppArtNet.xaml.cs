﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Asalabs.LightingControl.ArtNet;
using Asalabs.LigthingControl;

namespace TestAppArtNet
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ArtNetClient an = new ArtNetClient();
        List<ChannelValuePanel> channelValuePanels = new List<ChannelValuePanel>(); 
        public MainWindow()
        {
            InitializeComponent();
            an.DataReceived+=an_DataReceived;
            for (int i = 1; i <= 512; i++)
            {
                var cvp = new ChannelValuePanel {Channel = i};
                channelValuePanels.Add(cvp);
                WpChannels.Children.Add(cvp);
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            an.Dispose();
            base.OnClosing(e);
        }

        private void an_DataReceived(object sender, Asalabs.LightingControl.ArtNet.DmxDataArgs e)
        {
            Action write = (() =>
            {
                foreach (var d in e.Data)
                {
                    var res = channelValuePanels.Where(c => c.Channel == d.Channel).ToList();
                    if (res.Any())
                        if (d.Channel == 1)
                            res.First().Value = d.Value;
                        else
                            res.First().Value = d.Value;

                }
            });
            Dispatcher.BeginInvoke(write);
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            an.EnableDmxSend = TbtEnable.IsChecked == true;
        }
        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            an.Blackout = TbtBlackout.IsChecked == true;
        }

        private void SldChannel1_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            an.SetChannelValue(1, e.NewValue);
        }
        private void SldChannel2_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            an.SetChannelValue(2, e.NewValue);
        }
        private void SldChannel3_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            an.SetChannelValue(3, e.NewValue);
        }
        private void SldMaster_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            an.SetMasterValue(e.NewValue);
        }
    }
}
