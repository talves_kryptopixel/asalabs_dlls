﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Asalabs.Converters;
using Asalabs.Drawing;
using Asalabs.ErrorReport;
using Asalabs.PropertyData.Properties;

namespace Asalabs.PropertyData
{

    public class PropertyFunctions
    {
        public static object CreateObject(Type type)
        {
            return Activator.CreateInstance(type, true);
        }

        public static bool PropertyExists(object o, string property)
        {
            var exists = false;
            try
            {
                var t = o.GetType();
                var prop = t.GetProperty(property);
                if (prop != null)
                    exists = true;
            }
            catch (Exception ex)
            {
                Errors.Show("PropertyFunctions PropertyExists", ex, Errors.ErrorType.Message);
            }
            return exists;
        }

        public static bool PropertyExists(List<object> olist, string property)
        {
            return olist.Aggregate(true, (current, o) => current & PropertyExists(o, property));
        }

        public static object GetPropertyValue(object o, string property)
        {
            try
            {
                var t = o.GetType();
                var prop = t.GetProperty(property);
                var value = prop.GetValue(o, null);
                return value;
            }
            catch (Exception ex)
            {
                Errors.Show("GetProperty", ex, Errors.ErrorType.Message);
            }
            return null;
        }


        static string ExcludeAllButNumbers(string text)
        {
            var result = string.Empty;
            foreach (var c in text)
                if (NumbersAndSignals.Contains(c))
                    result += c;
            return result;
        }


        private const string NumbersAndSignals = "0123456789-,.";
        private const string Numbers = "0123456789-";

        public static bool SetPropertyValue(object o, string property, string pValue)
        {
            var sucess = false;
            object value = null;
            try
            {
                var t = o.GetType();
                var prop = t.GetProperty(property);
                if (prop.PropertyType == typeof(int))
                {
                    if (!string.IsNullOrEmpty(pValue) && Numbers.Contains(pValue[0]))
                        value = int.Parse(ExcludeAllButNumbers(pValue));
                }
                if (prop.PropertyType == typeof(double))
                {
                    if (!string.IsNullOrEmpty(pValue) && Numbers.Contains(pValue[0]))
                        value = double.Parse(ExcludeAllButNumbers(pValue));
                }
                if (prop.PropertyType == typeof(string))
                {
                    if (!string.IsNullOrEmpty(pValue))
                        value = pValue;
                }
                if (prop.PropertyType == typeof(Thickness))
                {
                    if (!string.IsNullOrEmpty(pValue) && Numbers.Contains(pValue[0]))
                        value = new Thickness(double.Parse(ExcludeAllButNumbers(pValue)));
                }
                if (prop.PropertyType == typeof(FontFamily))
                {
                    if (!string.IsNullOrEmpty(pValue))
                        value = new FontFamily(pValue);
                }
                if (prop.PropertyType == typeof(FontWeight))
                {
                    if (!string.IsNullOrEmpty(pValue))
                        value = Converter.StringToFontWeight(pValue);
                }
                if (prop.PropertyType == typeof(Brush))
                {
                    if (!string.IsNullOrEmpty(pValue))
                        value = Converter.StringToBrush(pValue);
                }
                if (prop.PropertyType.BaseType == typeof(Enum))
                {
                    if (!string.IsNullOrEmpty(pValue))
                        value = Enum.Parse(prop.PropertyType, pValue);
                }
                if (prop.PropertyType == typeof(TimeZoneInfo))
                {
                    if (!string.IsNullOrEmpty(pValue))
                        value = Converter.StringToTimeZoneInfo(pValue);
                }
                prop.SetValue(o, value, null);
                sucess = true;
            }
            catch (Exception ex)
            {
                Errors.Show("SetProperty",ex, Errors.ErrorType.Error);
            }

            return sucess;
        }

        public static object CloneObject(object source)
        {
            var tSource = source.GetType();
            var destination = CreateObject(tSource);
            CopyObjectProperties(source, ref destination);
            return destination;
        }

        public static void CopyObjectProperties(object source, ref object destination)
        {
            var tSource = source.GetType();
            var tDestination = destination.GetType();
            if (tDestination == tSource)
            {
                var piaS = tSource.GetProperties();
                var piaD = tDestination.GetProperties();

                foreach (var piS in piaS)
                {
                    foreach (var piD in piaD)
                        if (piS.Name == piD.Name)
                        {
                            object value = piS.GetValue(source, null);
                            try
                            {
                                piD.SetValue(destination, value, null);
                            }
                            catch (Exception ex)
                            {
                                Errors.Show("CopyObjectProperties",ex, Errors.ErrorType.Warning);
                            }
                            break;
                        }
                }
            }
        }

        public static UIElement CloneUiElement(UIElement source)
        {
            var tSource = source.GetType();
            var destination = CreateObject(tSource) as UIElement;
            CopyElement(source, ref destination);
            return destination;
        }

        public static bool CopyElement(UIElement source, ref UIElement destination)
        {
            var sucess = false;
            var tSource = source.GetType();
            var tDestination = destination.GetType();
            if (tDestination == tSource)
            {
                PropertyInfo PiS, PiD;
                var list = GetPropertyValue(source, "GenericPropertyList") as List<string>;
                if (list != null)
                    foreach (var s in list)
                    {
                        PiS = tSource.GetProperty(s);
                        PiD = tSource.GetProperty(s);
                        PiD.SetValue(destination, PiS.GetValue(source, null), null);
                    }
                try
                {
                    var ses = GetPropertyValue(source, "ExtendedPropertyList") as List<string>;
                    if (ses != null)
                        foreach (string s in ses)
                        {
                            PiS = tSource.GetProperty(s);
                            PiD = tSource.GetProperty(s);
                            PiD.SetValue(destination, PiS.GetValue(source, null), null);
                        }
                }
                catch (Exception) { }
                sucess = true;
            }
            return sucess;
        }


        public static ComboBox GetFontFamilyComboBox()
        {
            var cb = new ComboBox();
            foreach (FontFamily ff in Fonts.SystemFontFamilies)
                cb.Items.Add(ff.ToString());
            return cb;
        }

        public static ComboBox GetFontWeightComboBox()
        {
            var cb = new ComboBox();
            var myType = typeof(FontWeights);
            var props = new List<PropertyInfo>(myType.GetProperties());

            foreach (var prop in props)
            {
                var s = prop.ToString();
                var i = s.LastIndexOf(' ') + 1;
                cb.Items.Add(s.Substring(i));
            }
            return cb;
        }

        public static int StringToInteger(string str)
        {
            var i = 0;
            try { i = int.Parse(str); }
            catch (Exception) { }
            return i;
        }

        public static ImageBrush CreateTrasparentGrid()
        {
            var ib = new ImageBrush
            {
                ImageSource = Resources.grid.ToImageSource(),
                Stretch = Stretch.None,
                TileMode = TileMode.Tile,
                Viewport = new Rect(0, 0, 16, 16),
                ViewportUnits = BrushMappingMode.Absolute
            };
            return ib;
        }
    }
    
}
