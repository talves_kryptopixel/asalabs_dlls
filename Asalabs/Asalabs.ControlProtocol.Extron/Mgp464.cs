﻿namespace Asalabs.ControlProtocol.Extron
{

    public static class Mgp464
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input">Inputs from 1-19</param>
        /// <param name="output">Windows from 1-4 or 0 to All</param>
        public static string InputSelection(int input, int window)
        {
            string data = "";
            if (input > 0 && input <= 19)
                if (window >= 0 && window <= 4)
                {
                    data = input + "*" + window + "!";
                }
            return data;
        }
        public static string MuteWindow(int window, bool mute)
        {
            string data = "";
            if (window >= 0 && window <= 4)
            {
                if (mute)
                    data = window + "*1B";
                else
                    data = window + "*0B";
            }
            return data;
        }
        public static string GetWindowStatus(int window)
        {
            string data = "";
            if (window >= 0 && window <= 4)
            {
                data = window + "B";
            }
            return data;
        }
    }
}
