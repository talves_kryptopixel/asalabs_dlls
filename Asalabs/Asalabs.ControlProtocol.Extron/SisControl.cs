﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Asalabs.Net.Tcp;

namespace Asalabs.ControlProtocol.Extron
{
    public class SisControl
    {
            public IPAddress Ip { private set; get; }
            public int Port = 23;
            private Socket _clientSocket;
            public event EventHandler<TextEventArgs> DataReceived;

            public SisControl(IPAddress ip)
            {
                Ip = ip;
            }


            public class TextEventArgs : EventArgs
            {
                public string Text { set; get; }
            }

            void Message(string text)
            {
                if (DataReceived != null)
                    DataReceived(this, new TextEventArgs { Text = text });
            }

            public void Connect()
            {
                try
                {
                    _clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    _clientSocket.Connect(Ip, Port);
                    WaitForData();
                }
                catch (Exception ex)
                {
                    Message(ex.Message);
                }
            }
            public void Connect(IPAddress ip)
            {
                if (!Ip.Equals(ip))
                {
                    Ip = ip;
                    Disconnect();
                }
                Connect();
            }

            public void Disconnect()
            {
                if (_clientSocket != null)
                {
                    if (_clientSocket.Connected)
                        _clientSocket.Disconnect(false);
                    _clientSocket.Close();
                    _clientSocket.Dispose();
                    _clientSocket = null;
                }
            }

            byte[] _myDataBuffer = new byte[10];

            IAsyncResult _myResult;

            public AsyncCallback my_pfnCallBack;

        public void WaitForData()
        {
            try
            {
                if (my_pfnCallBack == null)
                {
                    my_pfnCallBack = new AsyncCallback(OnDataReceived);
                }
                var theSocPkt = new SocketPacket();
                theSocPkt.CurrentSocket = _clientSocket;
                // Start listening to the data asynchronously
                _myResult = _clientSocket.BeginReceive(theSocPkt.DataBuffer, 0, theSocPkt.DataBuffer.Length,
                    SocketFlags.None, my_pfnCallBack, theSocPkt);
            }
            catch (SocketException se)
            {
                Message("WaitForData : " + se.Message);
                if (!_clientSocket.Connected)
                    Connect();
            }
            catch (Exception ex)
            {
                Message("WaitForData : " + ex.Message);
                if (_clientSocket != null)
                    if (!_clientSocket.Connected)
                        Connect();
            }

        }

        public void OnDataReceived(IAsyncResult asyn)
            {
                try
                {
                    var theSockId = (SocketPacket)asyn.AsyncState;
                    var iRx = theSockId.CurrentSocket.EndReceive(asyn);
                    var chars = new char[iRx + 1];
                    var charLen = Encoding.ASCII.GetChars(theSockId.DataBuffer, 0, iRx, chars, 0);
                    var szData = new String(chars);
                    if (szData == "\0") theSockId.CurrentSocket.Disconnect(true);
                    else
                    {
                        Message(szData);
                    }
                }
                catch (ObjectDisposedException)
                {
                    Message("OnDataReceived: Socket has been closed\n");
                    if (_clientSocket!=null)
                        if (!_clientSocket.Connected)
                            Connect();
                }
                catch (SocketException se)
                {
                    Message("OnDataReceived : " + se.Message);
                    if (!_clientSocket.Connected)
                        Connect();
                }
                catch (Exception ex)
                {
                    Message("OnDataReceived : " + ex.Message);
                    if (!_clientSocket.Connected)
                        Connect();
                }
                finally
                {
                    WaitForData();
                }
            }

            public void Send(string data)
            {
                try
                {
                    if (_clientSocket != null)
                    {
                        if (!_clientSocket.Connected)
                            Connect();
                        if (_clientSocket.Connected)
                        {

                            var bytes = Encoding.ASCII.GetBytes(data);
                            _clientSocket.Send(bytes);
                            Message(data);
                        }
                    }
                }
                catch (SocketException se)
                {
                    Message(se.Message);
                }
                catch (Exception ex)
                {
                    Message(ex.Message);
                }
            }

            /*
            private void SendStream(string data)
            {
                try
                {
                    using (var client = new TcpClient())
                    {
                        client.Connect("192.168.12.80", 23);

                        if (client.Connected)
                        {
                            var stream = client.GetStream();

                            var send = Encoding.ASCII.GetBytes(data);

                            stream.Write(send, 0, send.Length);
                        }
                        //client.Close();
                    }
                }
                catch (SocketException sex)
                {
                    Message(sex.Message + "\n" + sex.StackTrace);
                }
                catch (Exception ex)
                {
                    Message(ex.Message + "\n" + ex.StackTrace);
                }
            }
            */

        }
}
