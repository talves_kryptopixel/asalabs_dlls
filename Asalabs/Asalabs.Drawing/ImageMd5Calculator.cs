﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Asalabs.Drawing
{
    internal class ImageMd5Calculator
    {
        private static string GenerateMd5(Image image)
        {
            // get the bytes from the image
            byte[] bytes = null;
            using (var ms = new MemoryStream())
            {
                image.Save(ms, ImageFormat.Gif); // gif for example
                bytes = ms.ToArray();
            }
            // hash the bytes
            var md5 = new MD5CryptoServiceProvider();
            byte[] hash = md5.ComputeHash(bytes);

            // make a hex string of the hash for display or whatever
            var sb = new StringBuilder();
            foreach (byte b in hash)
            {
                sb.Append(b.ToString("x2").ToLower());
            }
            return sb.ToString();
        }
    }
}
