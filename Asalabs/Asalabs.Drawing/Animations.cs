﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace AsaAnimations
{
    public static class Animations
    {
        public static void AnimateProperty(string elementName, DependencyProperty dependencyProperty, double startValue,
            double endValue, TimeSpan duration, bool autoReverse, RepeatBehavior repeat)
        {
            var da = new DoubleAnimation
            {
                Duration = new Duration(duration),
                From = startValue,
                To = endValue,
                AutoReverse = autoReverse,
                RepeatBehavior = repeat
            };
            var sb = new Storyboard();

            sb.Children.Add(da);
            Storyboard.SetTargetName(da, elementName);
            Storyboard.SetTargetProperty(da, new PropertyPath(dependencyProperty));
            sb.Begin();
        }


        public static void FadeOut(UIElement sender, double duration)
        {
            var animation = new DoubleAnimation
            {
                Duration = TimeSpan.FromMilliseconds(sender.Opacity*duration),
                From = sender.Opacity,
                To = 0
            };
            sender.BeginAnimation(UIElement.OpacityProperty, animation);
        }

        public static void FadeIn(UIElement sender, double duration)
        {
            var animation = new DoubleAnimation
            {
                Duration = TimeSpan.FromMilliseconds(sender.Opacity*duration),
                From = sender.Opacity,
                To = 1
            };
            sender.BeginAnimation(UIElement.OpacityProperty, animation);
        }
    }
}