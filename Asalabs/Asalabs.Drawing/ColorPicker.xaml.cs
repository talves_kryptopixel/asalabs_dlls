﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Asalabs.Converters;

namespace Asalabs.Drawing
{
    /// <summary>
    /// Interaction logic for ColorPicker.xaml
    /// </summary>
    public partial class ColorPicker
    {
        public ColorPicker()
        {
            InitializeComponent();
        }

        public event EventHandler<ColorArgs> ColorChanged;

        public Brush PreviousBrush{ set; get; }

        public Brush NewBrush { set; get; }

        public class ColorArgs : EventArgs
        {
            public Brush brush { set; get; }
        }
        
        int red = 0;
        public int Red
        {
            set
            {
                if (red != value && value >= 0 && value <= 255)
                {
                    red = value;
                    tbDecRed.Text = value.ToString();
                    tbHexRed.Text = Converter.DecToHex(value, 2);
                    sldRed.Value = value;
                    UpdatePreview();
                }
            }
            get { return red; }
        } 
        
        int green = 0;
        public int Green
        {
            set
            {
                if (green != value && value >= 0 && value <= 255)
                {
                    green = value;
                    tbDecGreen.Text = value.ToString();
                    tbHexGreen.Text = Converter.DecToHex(value, 2);
                    sldGreen.Value = value; UpdatePreview();
                }
            }
            get { return green; }
        } 
        
        int blue = 0; 
        public int Blue
        {
            set
            {
                if (blue != value && value >= 0 && value <= 255)
                {
                    blue = value;
                    tbDecBlue.Text = value.ToString();
                    tbHexBlue.Text = Converter.DecToHex(value, 2);
                    sldBlue.Value = value; UpdatePreview();
                }
            }
            get { return blue; }
        }
        
        int alpha = 0;
        public int Alpha
        {
            set
            {
                if (alpha != value && value >= 0 && value <= 255)
                {
                    alpha = value;
                    tbDecAlpha.Text = value.ToString();
                    tbHexAlpha.Text = Converter.DecToHex(value, 2);
                    sldAlpha.Value = value; UpdatePreview();
                }
            }
            get { return alpha; }
        }


        public void SetStartColor(string sColor)
        {
            bool fail = false;
            if (sColor.Length == 9 && sColor[0] == '#')
            {
                foreach (char c in sColor.Substring(1, 8))
                {
                    if (c < 48 || (c > 57 && c < 65) || c > 70)
                        fail = true;
                }
                if (!fail)
                {
                    Alpha = Convert.ToInt32(sColor.Substring(1, 2), 16);
                    Red = Convert.ToInt32(sColor.Substring(3, 2), 16);
                    Green = Convert.ToInt32(sColor.Substring(5, 2), 16);
                    Blue = Convert.ToInt32(sColor.Substring(7, 2), 16);
                }
            }
            PreviousBrush = NewBrush;
        }

        // To update the result color, text and preview rectange, and to send it to the selected element for preview purpose
        void UpdatePreview()
        {
            string result = "#" + tbHexAlpha.Text + tbHexRed.Text + tbHexGreen.Text + tbHexBlue.Text;
            tbResult.Text = result;
            NewBrush = Converter.StringToBrush(result);
            rtResult.Fill = NewBrush;

            if (ColorChanged != null)
            {
                ColorArgs ca = new ColorArgs();
                ca.brush = NewBrush;
                ColorChanged(this, ca);
            }
        }

        // To receive the slider event and convert the values to
        private void slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (sender == sldRed)
            {
                Red = (int)sldRed.Value;
            }
            if (sender == sldGreen)
            {
                Green = (int)sldGreen.Value;
            }
            if (sender == sldBlue)
            {
                Blue = (int)sldBlue.Value;
            }
            if (sender == sldAlpha)
            {
                Alpha = (int)sldAlpha.Value;
            }
        }


        private void btApply_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void tbDec_TextChanged(object sender, TextChangedEventArgs e)
        {
            if ((sender as TextBox).Text.Length > 0)
                try
                {
                    if (sender == tbDecRed)
                        Red = int.Parse(tbDecRed.Text);
                    if (sender == tbDecGreen)
                        Green = int.Parse(tbDecGreen.Text);
                    if (sender == tbDecBlue)
                        Blue = int.Parse(tbDecBlue.Text);
                    if (sender == tbDecAlpha)
                        Alpha = int.Parse(tbDecAlpha.Text);

                    TextBox tb = sender as TextBox;
                    int i = int.Parse(tb.Text);
                    if (i > 255)
                        i = 255;
                    tb.Text = i.ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("tbDec_TextChanged :\n" + ex.Message);
                }
        }

        private void tbHex_TextChanged(object sender, TextChangedEventArgs e)
        {
            if ((sender as TextBox).Text.Length == 2)
                try
                {
                    if (sender == tbHexRed)
                        Red = Convert.ToInt32(tbHexRed.Text, 16);
                    if (sender == tbHexGreen)
                        Green = Convert.ToInt32(tbHexGreen.Text, 16);
                    if (sender == tbHexBlue)
                        Blue = Convert.ToInt32(tbHexBlue.Text, 16);
                    if (sender == tbHexAlpha)
                        Alpha = Convert.ToInt32(tbHexAlpha.Text, 16);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("tbHex_TextChanged :\n" + ex.Message);
                }
        }


        private void tbHex_KeyDown(object sender, KeyEventArgs e)
        {
            var textBox = sender as TextBox;
            if(textBox != null && textBox.Text.Length==2)
                textBox.Text = "";
            e.Handled = !(((e.Key >= Key.D0 && e.Key <= Key.D9) || e.Key == Key.A || e.Key == Key.B || e.Key == Key.C || e.Key == Key.D || e.Key == Key.E || e.Key == Key.F || (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)));
        }

        private void tbDec_KeyDown(object sender, KeyEventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox != null && textBox.Text.Length == 3)
                textBox.Text = "";
            e.Handled = !(((e.Key >= Key.D0 && e.Key <= Key.D9) || (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)));
        }
    }
}
