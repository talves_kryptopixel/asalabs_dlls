﻿using System;
using System.Linq;
using System.Windows;
using System.Timers;

namespace Asalabs.Drawing
{
    /// <summary>
    /// Interaction logic for IdentifyWindow.xaml
    /// </summary>
    public partial class IdentifyWindow : Window
    {
        private readonly Timer _timer;
        public IdentifyWindow(System.Windows.Forms.Screen scr)
        {
            InitializeComponent();
            _timer = new Timer {AutoReset = false, Interval = 2000};
            _timer.Elapsed += t_Elapsed;
            System.Drawing.Rectangle r = scr.Bounds;
            Top = r.Top;
            Left = r.Left;
            Width = r.Width;
            Height = r.Height;
            var id="";
            const string numbers = "0123456789";
            foreach (char c in scr.DeviceName)
                if (numbers.Contains(c))
                    id += c;
            Id.Content = id;
        }

        public void ThreadSafeClose()
        {
            Action CloseAction = (() => this.Close());
            Dispatcher.BeginInvoke(CloseAction);
        }
        void t_Elapsed(object sender, ElapsedEventArgs e)
        {
            _timer.Stop();
            _timer.Enabled = false;
            ThreadSafeClose();  
        }

        public void ShowIdentifier()
        {            
            _timer.Enabled = true;
            _timer.Start();
            this.Show();   
        }

        private void Viewbox_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            _timer.Stop();
            _timer.Enabled = false;
            ThreadSafeClose();  
        }
    }
}
