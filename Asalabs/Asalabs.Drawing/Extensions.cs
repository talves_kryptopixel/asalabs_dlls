﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Asalabs.Drawing
{
    public static class BitmapExtensions
    {
        public static ImageSource ToImageSource(this System.Drawing.Bitmap bitmap)
        {
            var hbitmap = IntPtr.Zero;
            try
            {
                hbitmap = bitmap.GetHbitmap();
                var imageSource = Imaging.CreateBitmapSourceFromHBitmap(hbitmap, IntPtr.Zero, Int32Rect.Empty,
                    BitmapSizeOptions.FromWidthAndHeight(bitmap.Width, bitmap.Height));

                return imageSource;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                NativeMethods.DeleteObject(hbitmap);
            }
            return null;
        }
    }

    public static class NativeMethods
    {
        [DllImport("gdi32")]
        public static extern int DeleteObject(IntPtr hObject);
    }
}
