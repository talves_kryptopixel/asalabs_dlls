﻿using System;
using System.Windows;

namespace Asalabs.ErrorReport
{
    public static class Errors
    {
        public static ErrorType ShowErrors = ErrorType.Message;
        public enum ErrorType { None = 0, Exception = 1, Error = 2, Warning = 3, Message = 4 }
        public static void Show(string errorMessage, Exception ex, ErrorType eType)
        {
            if (eType <= ShowErrors)
            {
                var error = "Source : " + ex.Source + "\n";
                error += "Message : " + ex.Message + "\n";
                error += "Stack Trace : " + ex.StackTrace + "\n";
                MessageBox.Show(error, errorMessage);
            }
        }
    }
}