﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Asalabs.ControlProtocol.Symetrix
{
    public class UdpControl
    {
            public class TextArgs : EventArgs
            {
                public string Text { set; get; }
            }
            public class ValueArgs : EventArgs
            {
                public int ControlId { set; get; }
                public int Value { set; get; }
            }

            static UdpClient udpDevice = new UdpClient();
            static UdpState udpState = new UdpState();
            public bool IsConnected { get { return udpDevice.Client.Connected; } }
            public IPAddress Ip { private set; get; }
            public const int Port = 48630;
            public int updateinterval = 100;
            public event EventHandler<ValueArgs> ValueChanged;
            public event EventHandler<TextArgs> Message;

            public const int ON = 65535;
            public const int OFF = 0;
            public class SymControl
            {
                public string name;
                public int control;
                public int value = -1;
            }
            public class UdpState
            {
                public IPEndPoint e;
                public UdpClient u;
            }

            public UdpControl(IPAddress ip)
            {
                Ip = ip;
            }
            public void Connect(IPAddress ip)
            {
                //if (udpDevice.Client.Connected)
                //Disconnect();
                Ip = ip;
                Connect();
            }
            public void Connect()
            {
                try
                {
                    udpDevice.DontFragment = true;
                    udpDevice.Connect(Ip, Port);
                    if (udpDevice.Client.Connected)
                    {
                        WriteText("Connected\n");


                        // Receive a message and write it to the console.   
                        udpState.e = new IPEndPoint(Ip, Port);
                        udpState.u = udpDevice;

                        // Blocks until a message returns on this socket from a remote host.
                        udpDevice.BeginReceive(new AsyncCallback(ReceiveCallback), udpState);
                    }
                }
                catch (Exception ex)
                {
                    WriteText("Connect exception : " + ex.Message + "\n" + ex.StackTrace + "\n");
                }
            }

            private void ReceiveCallback(IAsyncResult ar)
            {
                try
                {
                    if (udpDevice != null)
                        if (udpDevice.Client != null)
                            if (udpDevice.Client.Connected)
                            {
                                UdpClient u = ((UdpState)(ar.AsyncState)).u;
                                IPEndPoint e = ((UdpState)(ar.AsyncState)).e;

                                Byte[] receiveBytes = u.EndReceive(ar, ref e);
                                string receiveString = Encoding.ASCII.GetString(receiveBytes);
                                WriteText(receiveString);
                                lock (buffer)
                                    buffer += receiveString;

                                ProcessData();

                                if (udpDevice.Client.Connected)
                                    u.BeginReceive(ReceiveCallback, ar.AsyncState);
                            }
                }
                catch (Exception ex)
                {
                    WriteText("ReceiveCallback exception : " + ex.Message + "\n" + ex.StackTrace + "\n");
                }
            }

            string buffer = "";
            void ProcessData()
            {
                lock (buffer)
                {
                    while (buffer.Contains("#"))
                    {
                        SymControl sc = new SymControl();

                        buffer = buffer.Substring(buffer.IndexOf("#") + 1);
                        int eqIndex = buffer.IndexOf("=");
                        int crIndex = buffer.IndexOf("\r");
                        string control = buffer.Substring(0, eqIndex);
                        string value = buffer.Substring(eqIndex + 1, crIndex - eqIndex - 1);

                        sc.control = int.Parse(control);
                        sc.value = int.Parse(value);

                        WriteText("control(" + control + "):value(" + value + ")\n");
                        WriteData(sc.control, sc.value);

                    }
                }
            }

            public string ChangeUpdateInterval(int i)
            {
                if (i > 100 && i < 100000)
                {
                    updateinterval = i;
                    SendCommand("PUI " + updateinterval);
                    return "Ok";
                }
                else
                    return "Enter a number between 100 and 100000ms";
            }

            /*
            public void Disconnect()
            {
                if (udpDevice != null)
                    if (udpDevice.Client.Connected)
                    {
                        udpDevice.Client.DisconnectAsync(new SocketAsyncEventArgs());
                        WriteText("Disconnected");
                    }
            }
            public void Dispose()
            {
                if (udpDevice != null)
                    if (udpDevice.Client.Connected)
                    {
                        udpDevice.Client.DisconnectAsync(new SocketAsyncEventArgs());
                        udpDevice.Close();
                        WriteText("Disconnected");
                    }
            }
            */
            public void SendCommand(string stCmd)
            {
                if (udpDevice != null)
                    if (udpDevice.Client != null)
                        if (udpDevice.Client.Connected)
                        {
                            // Sends a message to the host to which you have connected.
                            Byte[] sendBytes = Encoding.ASCII.GetBytes(stCmd + "\r");

                            udpDevice.Send(sendBytes, sendBytes.Length);
                        }
            }

            void WriteText(string text)
            {
                if (Message != null)
                    Message(this, new TextArgs { Text = text });
            }

            void WriteData(int control, int value)
            {
                if (ValueChanged != null)
                    ValueChanged(this, new ValueArgs { ControlId = control, Value = value });
            }
     
    }
}
