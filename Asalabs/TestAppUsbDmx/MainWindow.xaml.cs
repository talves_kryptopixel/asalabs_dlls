﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Asalabs.LightingControl.EnttecOpenUsbDmx;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Asalabs.Extensions;

namespace TestAppUsbDmx
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        readonly DmxController _dmxController = new DmxController();
        public MainWindow()
        {
            InitializeComponent();
            _dmxController.StatusChanged += DmxController_StatusChanged;
        }
        void DmxController_StatusChanged(object sender, StringArgs e)
        {
            Action showMessage = (() =>
            {
                MessageBox.Show(e.Value, sender.ToString());
            });
            Dispatcher.BeginInvoke(showMessage);
        }
    
        private void RangeBase1_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            _dmxController.SetChannelValue(1, e.NewValue);
        }
        private void RangeBase2_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            _dmxController.SetChannelValue(2, e.NewValue);
        }
        private void RangeBase3_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            _dmxController.SetChannelValue(3, e.NewValue);
        }
        private void RangeBase4_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            _dmxController.SetMasterValue(e.NewValue);
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            _dmxController.OpenDmx.Blackout = TbtBlackout.IsChecked == true;
        }

        private void TbtEnable_OnClick(object sender, RoutedEventArgs e)
        {
            _dmxController.OpenDmx.Active = TbtEnable.IsChecked == true;
        }
    }
}
