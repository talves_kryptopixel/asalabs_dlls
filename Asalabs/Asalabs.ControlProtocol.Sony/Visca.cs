﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using Asalabs.Extensions;

namespace Asalabs.ControlProtocol.Sony
{
    public enum CommandOrigins { Controller, Pc };
    public enum ActionTypes { Command, Inquiry };
    public enum DSubOutputTypes { Rgb, Yuv, RgbSyncOff, RgbSyncRgb};
    public enum ViscaSpeeds { Spd9600 = 9600, Spd38400 = 38400 };
    public enum DataTypes { None, Id, Power, Version, PanTiltPosition, ZoomPosition, FocusPosition, IrisPosition, Tally };

    public class StackDataArgs : EventArgs
    {
        public StackData Data { set; get; }
    }

    public class StackData
    {
        public CommandOrigins CommandOrigin { set; get; }
        public ActionTypes ActionType { set; get; }
        public DataTypes DataType { set; get; }
        public byte[] SendData { set; get; }
        public List<byte[]> ReceivedData { set; get; }
        public List<byte[]> AnswerArgs { set; get; }
        public StackData()
        {
            ReceivedData = new List<byte[]>();
            AnswerArgs = new List<byte[]>();
        }
        public int Value { set; get; }
        public int CameraId { set; get; }
    }



    /// <summary>
    /// [Deprecated] Use ViscaBridge insted.
    /// </summary>
    public class ViscaPort : IDisposable
    {
        public void Dispose()
        {
            _timer.Enabled = false;
            _timer.Stop();
            _timer.Close();
            _timer.Dispose();
        }

        private readonly System.Timers.Timer _timer = new System.Timers.Timer(50);




        private readonly SerialPort _spController;
        private readonly SerialPort _spCamera;
        private readonly object _locker = new object();
        private readonly List<byte> buffer = new List<byte>();
        private readonly List<StackData> VCStack = new List<StackData>();

        public void ClearBuffer()
        {
            VCStack.Clear();
        }


        public event EventHandler<StackDataArgs> DataReceived;
        public event EventHandler<StringArgs> MessageReceived;
        public event EventHandler<StringArgs> ErrorReceived;

        private readonly bool _systemReady = false;

        public ViscaPort(int inPort, int outPort, ViscaSpeeds speed)
        {
            var _inPort = "COM" + inPort;
            var _outPort = "COM" + outPort;
            string[] ports = SerialPort.GetPortNames();
            if (!ports.Contains(_inPort) || !ports.Contains(_outPort)) return;
            var _speed = (int)speed;
            _timer.AutoReset = true;
            _timer.Elapsed += timer_Elapsed;
            _timer.Enabled = true;
            _timer.Start();
            _spController = new SerialPort(_inPort, _speed, Parity.None, 8, StopBits.One);
            _spCamera = new SerialPort(_outPort, _speed, Parity.None, 8, StopBits.One);
            _spController.DataReceived += _spController_DataReceived;
            _spCamera.DataReceived += _spCamera_DataReceived;
            _systemReady = true;
        }

        void MessageSend(string s)
        {
            if (ErrorReceived != null)
                ErrorReceived(this, new StringArgs { Value = s });
        }

        private bool _commandExecuting = false;
        void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (VCStack.Count > 0 && !_commandExecuting)
                {
                    var sd = VCStack[0];
                    _spCamera.Write(sd.SendData, 0, sd.SendData.Length);
                    if (MessageReceived != null)
                        MessageReceived(this,
                            new StringArgs
                            {
                                Value = "Input Data received : " + BitConverter.ToString(sd.SendData)
                            });
                    if (sd.ActionType == ActionTypes.Command)
                        VCStack.RemoveAt(0);
                }
            }
            catch (Exception ex)
            {
                MessageSend(ex.Message);
            }

        }
        public void Open() {
            try
            {
                if (_systemReady)
                    _spController.Open();
            }
            catch (Exception ex)
            {
                MessageSend(ex.Message);
            }
            try
            {
                if (_systemReady)
                    _spCamera.Open();
            }
            catch (Exception ex)
            {
                MessageSend(ex.Message);
            }
        }
        public void Close()
        {
            _timer.Stop();
            _timer.Enabled = false;
            _timer.Close();
            _timer.Dispose();
            VCStack.Clear();
            if (_spController != null)
            {
                _spController.DataReceived-=_spController_DataReceived; 
                if (_spController.IsOpen)
                    _spController.Close();
                _spController.Dispose();
            }
            if (_spCamera != null)
            {
                _spCamera.DataReceived-=_spCamera_DataReceived;
                if (_spCamera.IsOpen)
                    _spCamera.Close();
                _spCamera.Dispose();
            }
        }

        //int StackIndex = 0;
        private void StackAdd(StackData sd)
        {
            VCStack.Add(sd);
        }


        private readonly List<byte[]> _receivedControllerCommands = new List<byte[]>();

        private void _spController_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var bytes = new byte[_spController.BytesToRead];
            _spController.Read(bytes, 0, _spController.BytesToRead);
            SplitControllerCommands(bytes);
        }

        private void SplitControllerCommands(IEnumerable<byte> bytes)
        {
            lock (_locker)
            {
                try
                {
                    foreach (var b in bytes)
                        buffer.Add(b);
                    while (buffer.Contains(0xFF))
                    {
                        var index = buffer.IndexOf(0xFF) + 1;
                        _receivedControllerCommands.Add(buffer.GetRange(0, index).ToArray());
                        buffer.RemoveRange(0, index);
                    }
                    while (_receivedControllerCommands.Count > 0)
                    {
                        var data = _receivedControllerCommands[0];
                        if (data.Length > 1)
                        {
                            if (data[1] == 0x09)
                                ProcessControllerInquiry(data);
                            else
                                ProcessControllerCommand(data);
                        }
                        _receivedControllerCommands.RemoveAt(0);
                    }
                }
                catch (Exception ex)
                {
                    MessageSend(ex.Message);
                }
            }
        }

        private void ProcessControllerCommand(byte[] data)
        {
            try
            {
                _spCamera.Write(data, 0, data.Length);
               /* if (data.Length == 8)
                    if (data[1] == 0x01 && data[2] == 0x7E && data[3] == 0x01 && data[4] == 0x0A && data[5] == 0x00 &&
                        data[6] == 0x02)
                        if (DataReceived != null)
                            DataReceived(this,
                                new StackDataArgs
                                {
                                    Data =
                                        new StackData
                                        {
                                            ActionType = ActionTypes.Command,
                                            DataType = DataTypes.Tally,
                                            IValue = GetCommandId(data[0])
                                        }
                                });*/
                if (MessageReceived != null)
                    MessageReceived(this,
                        new StringArgs {Value = "Input Data received : " + BitConverter.ToString(data)});
            }
            catch (Exception ex)
            {
                MessageSend(ex.Message);
            }
        }

        private void ProcessControllerInquiry(byte[] data)
        {
            try
            {
                if (data.Length == 5)
                {
                    if (data[2] == 0x04 && data[3] == 0x00)
                    {
                        StackAdd(new StackData
                        {
                            CommandOrigin = CommandOrigins.Controller,
                            ActionType = ActionTypes.Inquiry,
                            DataType = DataTypes.Power,
                            SendData = data
                        });
                        return;
                    }
                    if (data[2] == 0x00 && data[3] == 0x02)
                    {
                        StackAdd(new StackData
                        {
                            CommandOrigin = CommandOrigins.Controller,
                            ActionType = ActionTypes.Inquiry,
                            DataType = DataTypes.Version,
                            SendData = data
                        });
                        return;
                    }
                    StackAdd(new StackData
                    {
                        CommandOrigin = CommandOrigins.Controller,
                        ActionType = ActionTypes.Inquiry,
                        DataType = DataTypes.None,
                        SendData = data
                    });
                }
            }
            catch (Exception ex)
            {
                MessageSend(ex.Message);
            }

        }

        private readonly List<byte[]> _receivedCameraCommands = new List<byte[]>();
        private void _spCamera_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                var bytes = new byte[_spCamera.BytesToRead];
                _spCamera.Read(bytes, 0, _spCamera.BytesToRead);
                SplitCameraCommands(bytes);
            }
            catch (Exception ex)
            {
                MessageSend(ex.Message);
            }

        }


        private void SplitCameraCommands(IEnumerable<byte> bytes)
        {

            lock (_locker)
            {
                try
                {
                    foreach (var b in bytes)
                        buffer.Add(b);
                    while (buffer.Contains(0xFF))
                    {
                        var index = buffer.IndexOf(0xFF) + 1;
                        _receivedCameraCommands.Add(buffer.GetRange(0, index).ToArray());
                        buffer.RemoveRange(0, index);
                    }
                    while (_receivedCameraCommands.Count > 0)
                    {
                        var data = _receivedCameraCommands[0];
                        if (MessageReceived != null)
                            MessageReceived(this, new StringArgs { Value = "Ouput Data received : " + BitConverter.ToString(data) });
                        if (data.Length > 1)
                        {
                            if (data[0] >= 0x90 && data[0] <= 0xF0)
                            {
                                if (data[1] == 0x50) ProcessOutputCommands(data);
                                var s = "";
                                if (data[1] >= 0x41 && data[1] <= 0x47)
                                {
                                    s += " ACK ";
                                    _commandExecuting = true;
                                }
                                if (data[1] >= 0x51 && data[1] <= 0x57)
                                {
                                    s += " Complete ";
                                    //Get_PanTilt_AbsolutePosition(GetResponseId(data[0]));
                                    //Get_Zoom_Position(GetResponseId(data[0]));
                                    //Get_Focus_Position(GetResponseId(data[0]));
                                    //Get_Iris_Position(GetResponseId(data[0]));
                                    _commandExecuting = false;
                                }
                                if (s != "")
                                    if (MessageReceived != null)
                                        MessageReceived(this, new StringArgs { Value = s });
                            }
                            if (data[1] == 0x09)
                                for (int i = 0; i < VCStack.Count; i++)
                                {
                                    var res = data.SequenceEqual(VCStack[i].SendData);
                                    if (res)
                                        VCStack.RemoveAt(i);
                                }
                        }


                        if (_spController.IsOpen)
                            _spController.Write(data, 0, data.Length);

                        _receivedCameraCommands.RemoveAt(0);
                    }
                }
                catch (Exception ex)
                {
                    MessageSend(ex.Message);
                }
            }
        }

        private void ProcessOutputCommands(IList<byte> bytes)
        {
            try
            {
                string s = "";
                if (VCStack.Count > 0)
                {
                    switch (VCStack[0].ActionType)
                    {
                        case ActionTypes.Inquiry:
                            switch (VCStack[0].DataType)
                            {
                                case DataTypes.PanTiltPosition:
                                    if (bytes.Count >= 12)
                                    {
                                        s += " Position ";
                                        foreach (var b in bytes)
                                            s += BitConverter.ToString(new byte[] { b }) + " ";
                                        var pan = new byte[5];
                                        for (var i = 0; i < 5; i++)
                                        {
                                            pan[i] = bytes[i + 2];
                                        }
                                        if (MessageReceived != null)
                                            MessageReceived(this,
                                                new StringArgs
                                                {
                                                    Value = "Data received : " + BitConverter.ToString(pan)
                                                });
                                        VCStack[0].AnswerArgs.Add(pan);
                                        var tilt = new byte[4];
                                        for (var i = 0; i < 4; i++)
                                        {
                                            tilt[i] = bytes[i + 7];
                                        }
                                        if (MessageReceived != null)
                                            MessageReceived(this,
                                                new StringArgs
                                                {
                                                    Value = "Data received : " + BitConverter.ToString(tilt)
                                                });
                                        VCStack[0].AnswerArgs.Add(tilt);
                                    }
                                    break;
                                case DataTypes.ZoomPosition:
                                    if (bytes.Count >= 7)
                                    {
                                        s += " Position ";
                                        foreach (var b in bytes)
                                            s += BitConverter.ToString(new byte[] { b }) + " ";
                                        var zoom = new byte[4];
                                        for (var i = 0; i < 4; i++)
                                        {
                                            zoom[i] = bytes[i + 2];
                                        }
                                        VCStack[0].AnswerArgs.Add(zoom);
                                    }
                                    break;
                                case DataTypes.FocusPosition:
                                    if (bytes.Count >= 7)
                                    {
                                        s += " Position ";
                                        foreach (var b in bytes)
                                            s += BitConverter.ToString(new byte[] { b }) + " ";
                                        var focus = new byte[4];
                                        for (var i = 0; i < 4; i++)
                                        {
                                            focus[i] = bytes[i + 2];
                                        }
                                        VCStack[0].AnswerArgs.Add(focus);
                                    }
                                    break;
                                case DataTypes.IrisPosition:
                                    if (bytes.Count >= 7)
                                    {
                                        s += " Position ";
                                        foreach (var b in bytes)
                                            s += BitConverter.ToString(new byte[] { b }) + " ";
                                        var iris = new byte[4];
                                        for (var i = 0; i < 2; i++)
                                        {
                                            iris[i] = bytes[i + 4];
                                        }
                                        VCStack[0].AnswerArgs.Add(iris);
                                    }
                                    break;
                            }
                            if (DataReceived != null)
                            {
                                var args = new StackData
                                {
                                    CameraId = Helper.GetResponseId(bytes[0]),
                                    DataType = VCStack[0].DataType
                                };
                                foreach (var v in VCStack[0].AnswerArgs)
                                    args.AnswerArgs.Add(v);
                                DataReceived(this, new StackDataArgs { Data = args });
                            }
                            break;
                    }
                }
                if (VCStack.Count > 0)
                {
                    VCStack.RemoveAt(0);
                }
                if (MessageReceived != null)
                    MessageReceived(this, new StringArgs { Value = "Stack Count = " + VCStack.Count });
            }
            catch (Exception ex)
            {
                MessageSend(ex.Message);
            }
        }

        #region Inicialization
        public void Get_Power(int camId)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x09, 0x04, 0x00, 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Inquiry, DataType= DataTypes.Power, SendData = bytes });
        }

        #endregion

        #region PanTilt Drive
        /// <summary>
        /// Camera PanTilt Up Command.
        /// Note : Send Stop command after release button or joystick
        /// </summary>
        /// <param name="camId">Camera Id : 1-7</param>
        /// <param name="panSpeed">Pan Speed : 0-24</param>
        /// <param name="tiltSpeed">Tilt Speed : 0-24</param>
        public void Set_PanTiltDrive_Up(int camId, int panSpeed = 14, int tiltSpeed = 14)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x06, 0x01, Helper.GetPanTiltSpeed(panSpeed), Helper.GetPanTiltSpeed(tiltSpeed), 0x03, 0x01, 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }
        public void Set_PanTiltDrive_Down(int camId, int panSpeed = 14, int tiltSpeed = 14)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x06, 0x01, Helper.GetPanTiltSpeed(panSpeed), Helper.GetPanTiltSpeed(tiltSpeed), 0x03, 0x02, 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }
        public void Set_PanTiltDrive_Left(int camId, int panSpeed = 14, int tiltSpeed = 14)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x06, 0x01, Helper.GetPanTiltSpeed(panSpeed), Helper.GetPanTiltSpeed(tiltSpeed), 0x01, 0x03, 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }
        public void Set_PanTiltDrive_Right(int camId, int panSpeed = 14, int tiltSpeed = 14)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x06, 0x01, Helper.GetPanTiltSpeed(panSpeed), Helper.GetPanTiltSpeed(tiltSpeed), 0x02, 0x03, 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }
        public void Set_PanTiltDrive_UpLeft(int camId, int panSpeed = 14, int tiltSpeed = 14)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x06, 0x01, Helper.GetPanTiltSpeed(panSpeed), Helper.GetPanTiltSpeed(tiltSpeed), 0x01, 0x01, 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }
        public void Set_PanTiltDrive_UpRight(int camId, int panSpeed = 14, int tiltSpeed = 14)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x06, 0x01, Helper.GetPanTiltSpeed(panSpeed), Helper.GetPanTiltSpeed(tiltSpeed), 0x02, 0x01, 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }
        public void Set_PanTiltDrive_DownLeft(int camId, int panSpeed = 14, int tiltSpeed = 14)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x06, 0x01, Helper.GetPanTiltSpeed(panSpeed), Helper.GetPanTiltSpeed(tiltSpeed), 0x01, 0x02, 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }
        public void Set_PanTiltDrive_DownRight(int camId, int panSpeed = 14, int tiltSpeed = 14)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x06, 0x01, Helper.GetPanTiltSpeed(panSpeed), Helper.GetPanTiltSpeed(tiltSpeed), 0x02, 0x02, 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }
        public void Set_PanTiltDrive_Stop(int camId, int panSpeed = 14, int tiltSpeed = 14)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x06, 0x01, Helper.GetPanTiltSpeed(panSpeed), Helper.GetPanTiltSpeed(tiltSpeed), 0x03, 0x03, 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }
        public void Set_PanTiltDrive_AbsolutePosition(int camId, int speed, byte[] pan, byte[] tilt)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x06, 0x02, Helper.GetPanTiltSpeed(speed), 0x00, pan[0], pan[1], pan[2], pan[3], pan[4],
                             tilt[0], tilt[1], tilt[2], tilt[3], 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }
        public void Set_PanTiltDrive_RelativePosition(int camId, int speed, byte[] pan, byte[] tilt)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x06, 0x03, Helper.GetPanTiltSpeed(speed), 0x00, pan[0], pan[1], pan[2], pan[3], pan[4],
                             tilt[0], tilt[1], tilt[2], tilt[3], 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }
        private void Set_PanTiltDrive_Home(int camId)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x06, 0x04, 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }
        private void Set_PanTiltDrive_Reset(int camId, int speed)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x06, 0x05, 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }
        public void Get_PanTilt_AbsolutePosition(int camId)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x09, 0x06, 0x12, 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Inquiry, DataType= DataTypes.PanTiltPosition, SendData = bytes });
        }

        #endregion

        #region Zoom Position
        /// <summary>
        /// Camera PanTilt Up Command.
        /// Note : Send Stop command after release button or joystick
        /// </summary>
        /// <param name="camId">Camera Id : 1-7</param>
        /// <param name="zoomSpeed">Pan Speed : 0-7</param>   
        public void Set_Zoom_Tele(int camId, int zoomSpeed = 4)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x04, 0x07, Helper.GetZoomFocusSpeed(zoomSpeed, 0x20), 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }
        public void Set_Zoom_Wide(int camId, int zoomSpeed = 4)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x04, 0x07, Helper.GetZoomFocusSpeed(zoomSpeed, 0x30), 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }
        public void Set_Zoom_Stop(int camId)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x04, 0x07, 0x00, 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }
        public void Set_Zoom_Position(int camId, byte[] zoom)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x04, 0x47, zoom[0], zoom[1], zoom[2], zoom[3], 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }
        public void Get_Zoom_Position(int camId)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x09, 0x04, 0x47, 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Inquiry, DataType= DataTypes.ZoomPosition, SendData = bytes });
        }
        #endregion
        #region Focus Position
        /// <summary>
        /// Camera PanTilt Up Command.
        /// Note : Send Stop command after release button or joystick
        /// </summary>
        /// <param name="camId">Camera Id : 1-7</param>
        /// <param name="focusSpeed">Pan Speed : 0-7</param>   
        public void Set_Focus_Far(int camId, int focusSpeed = 4)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x04, 0x08, Helper.GetZoomFocusSpeed(focusSpeed, 0x20), 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }
        public void Set_Focus_Near(int camId, int focusSpeed = 4)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x04, 0x08, Helper.GetZoomFocusSpeed(focusSpeed, 0x30), 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }
        public void Set_Focus_Stop(int camId)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x04, 0x08, 0x00, 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }
        public void Set_Focus_Position(int camId, byte[] focus)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x04, 0x48, focus[0], focus[1], focus[2], focus[3], 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }
        public void Get_Focus_Position(int camId)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x09, 0x04, 0x48, 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Inquiry, DataType = DataTypes.FocusPosition, SendData = bytes });
        }
        #endregion
        #region Iris Position
        /// <summary>
        /// Camera Iris Up Command.
        /// Note : Send Stop command after release button or joystick
        /// </summary>
        /// <param name="camId">Camera Id : 1-7</param> 
        public void Set_Iris_Up(int camId)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x04, 0x0B, 0x02, 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }
        public void Set_Iris_Down(int camId)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x04, 0x0B, 0x03, 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }
        public void Set_Iris_Reset(int camId)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x04, 0x0B, 0x00, 0x00, 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }

        public void Set_Iris_Position(int camId, byte[] iris)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x04, 0x4B, 0x00, 0x00, iris[0], iris[1], 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }

        public void Get_Iris_Position(int camId)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x09, 0x04, 0x4B, 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Inquiry, DataType = DataTypes.IrisPosition, SendData = bytes });
        }
        #endregion
        public void Set_DSubOutput(int camId, DSubOutputTypes type)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x7E, 0x01, 0x07, 0x0, 0x00, 0xFF };
            switch (type)
            {
                case DSubOutputTypes.Rgb: break;
                case DSubOutputTypes.Yuv: bytes[6] = 0x01; break;
                case DSubOutputTypes.RgbSyncOff: bytes[4] = 0x07; break;
                case DSubOutputTypes.RgbSyncRgb: bytes[4] = 0x07; bytes[6] = 0x01; break;
            }
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }


        public void Set_Gain_Limit(int camId, byte gainLimit)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x01, 0x04, 0x4C, 0x00, 0x00, gainLimit, 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Command, SendData = bytes });
        }

        public void Get_Gain_Limit(int camId)
        {
            byte[] bytes = { Helper.GetCommandId(camId), 0x09, 0x04, 0x4B, 0xFF };
            StackAdd(new StackData { CommandOrigin = CommandOrigins.Pc, ActionType = ActionTypes.Inquiry, DataType = DataTypes.IrisPosition, SendData = bytes });
        }
    }

    public static class Helper
    {

        public static byte GetCommandId(int i)
        {
            return (byte) (0x80 + i);
        }

        public static int GetCommandId(byte b)
        {
            return (int) (b - 0x80);
        }

        public static int GetResponseId(byte b)
        {
            int i = 1;
            var a = b - 0x90;
            while (a > 0)
            {
                a -= 0x10;
                i++;
            }
            return i;
        }

        public static byte GetPanTiltSpeed(int speed)
        {
            var _speed = speed;
            if (_speed > 24) _speed = 24;
            if (speed < 0) _speed = 0;
            return BitConverter.GetBytes(_speed)[0];
        }

        public static byte GetZoomFocusSpeed(int speed, byte byteBase)
        {
            var _speed = speed;
            if (_speed > 7) _speed = 7;
            if (speed < 0) _speed = 0;
            byte b = BitConverter.GetBytes(_speed)[0];
            b += byteBase;
            return b;
        }
    }
}
