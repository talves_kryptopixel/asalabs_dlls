﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace Asalabs.ControlProtocol.Sony
{

    public enum Source
    {
        Controller,
        Pc
    }

    public class Command
    {
        public Source Source;
        public byte[] Data = null;

        public Command(byte[] data, Source source = Source.Controller)
        {
            Source = source;
            Data = data;
        }
    }

    public class ViscaBridge : IDisposable
    {
        private readonly TimeoutTimer _commandExecuteTimeOutTimer = new TimeoutTimer();
        private readonly ViscaSerialPort _controllerPort;
        private readonly ViscaSerialPort _cameraPort;
        private readonly Queue<Command> _commandBuffer = new Queue<Command>();

        public ViscaBridge(int controllerPort, int cameraPort, ViscaSpeeds speed)
        {
            _controllerPort = new ViscaSerialPort(controllerPort, speed);
            _cameraPort = new ViscaSerialPort(cameraPort, speed);
        }







        public void Dispose()
        {
            _commandExecuteTimeOutTimer.Dispose();
            _controllerPort.Dispose();
            _cameraPort.Dispose();
        }
    }

    public class TimeoutTimer : IDisposable
    {
        private readonly Timer _timer = new Timer();
        public event ElapsedEventHandler TimeOutEnded;
        public volatile bool IsCounting = false;
        public TimeoutTimer(int timeout = 500)
        {
            _timer.AutoReset = false;
            _timer.Interval = timeout;
            _timer.Elapsed += Timer_Elapsed;
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!IsCounting) return;
            IsCounting = false;
            if (TimeOutEnded != null)
                TimeOutEnded(this, e);
        }
        public void Start()
        {
            if (IsCounting) return;
            _timer.Enabled = true;
            _timer.Start();
            IsCounting = true;
        }

        public void Dispose()
        {
            _timer.Elapsed -= Timer_Elapsed;
            _timer.Enabled = false;
            _timer.Stop();
            _timer.Dispose();
        }
    }
}
