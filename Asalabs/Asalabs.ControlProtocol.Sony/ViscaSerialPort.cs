﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using Asalabs.Extensions;

namespace Asalabs.ControlProtocol.Sony
{
    public class ViscaSerialPort : IDisposable
    {
        public event EventHandler DataReceived;
        public event EventHandler<StringArgs> ErrorReceived;
        private readonly SerialPort _sp = new SerialPort();
        private const string Com = "COM";

        private readonly Queue<byte> _serialBuffer = new Queue<byte>();

        public ViscaSerialPort(int portNumber, ViscaSpeeds speed)
        {
            _sp.PortName = Com + portNumber;
            _sp.BaudRate = (int)speed;
            _sp.Parity = Parity.None;
            _sp.Handshake = Handshake.None;
            _sp.StopBits = StopBits.One;
            _sp.DataBits = 8;
            _sp.DataReceived += _sp_DataReceived;
            _sp.ErrorReceived += _sp_ErrorReceived;
        }

        void _sp_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            ErrorEventThrow(e.EventType.ToString());
        }

        void _sp_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var data = new char[_sp.BytesToRead];
            _sp.Read(data, 0, _sp.BytesToRead);
            foreach (var c in data)
                _serialBuffer.Enqueue((byte)c);
            ProcessData();
        }

        private readonly object _locker = new object();
        private readonly Queue<byte[]> _commandBuffer = new Queue<byte[]>();
        void ProcessData()
        {
            lock (_locker)
            {
                while (_serialBuffer.Contains(0xFF))
                {
                    byte value;
                    var command = new Queue<byte>();
                    do
                    {
                        value = _serialBuffer.Dequeue();
                        command.Enqueue(value);
                    } while (value != 0xFF);
                    _commandBuffer.Enqueue(command.ToArray());
                }
            }
            while (_commandBuffer.Count > 0)
            {
                var data = _commandBuffer.Dequeue();
                if (data[1] == 0x09)
                {
                    //ProcessControllerInquiry(data);
                }
                else
                {
                    //ProcessControllerCommand(data);
                }
            }
        }

        private void ErrorEventThrow(string error)
        {
            if (ErrorReceived != null)
                ErrorReceived(this, new StringArgs { Value = error });
        }



        void DataReveivedEventThrow()
        {
            if (DataReceived != null)
                DataReceived(this, new EventArgs());
        }

        public void Dispose()
        {
        }
    }

}
