﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Asalabs.Extensions;

namespace Asalabs.LightingControl.ArtNet
{

        public class DmxChannel
        {
            public int Channel { set; get; }
            public int Value { set; get; }
        }

        public class DmxDataArgs
        {
            public List<DmxChannel> Data = new List<DmxChannel>();
        }

    public class ArtNetClient : IDisposable
    {
        public event EventHandler<StringArgs> ErrorReceived;
        public event EventHandler<DmxDataArgs> DataReceived;
        private readonly ArtDmx _artDmx = new ArtDmx();



        readonly Sender _udpSender = new Sender();
        readonly Listener _udpListener = new Listener();
        private readonly Thread _backGroundThread;
        private bool _isClosing = false;

        public ArtNetClient()
        {
            _udpListener.StartListening();
            _udpListener.DataReceived += _udpListener_DataReceived;
            _udpListener.ErrorReceived += _udpListener_ErrorReceived;
            _backGroundThread= new Thread(SendDmx);
            _backGroundThread.Start();
            EnableDmxSend = false;
        }

        void _udpListener_ErrorReceived(object sender, StringArgs e)
        {
            if (ErrorReceived != null)
                ErrorReceived(this, new StringArgs { Value = e.Value });
        }

        void _udpListener_DataReceived(object sender, DmxDataArgs e)
        {
            if (DataReceived != null)
                DataReceived(this, e );
        }

        public void SetChannelValue(int channel, double value)
        {
            _artDmx.SetChannelValue(channel,value);
        }
        public void SetMasterValue(double value)
        {
            _artDmx.SetMasterValue(value);
        }

        public void Dispose()
        {
            _isClosing = true;
            _backGroundThread.Abort();
            _udpSender.Dispose();
            _udpListener.StopListening();
            _udpListener.Dispose();
        }

        public bool Blackout { set; get; }
        public bool EnableDmxSend { set; get; }

        private void SendDmx()
        {
            while (!_isClosing)
            {
                try
                {
                    if (EnableDmxSend)
                    {
                        _udpSender.Send(Blackout ? _artDmx.BlackoutData : _artDmx.Data);
                    }
                }
                catch (Exception ex)
                {
                    Thread.Sleep(5000);
                }
                Thread.Sleep(32);
            }
        }

        public class ArtDmx
        {
            private byte[] Id = {0x41, 0x72, 0x74, 0x2D, 0x4E, 0x65, 0x74, 0x00}; // Equivalent to "Art-Net"+0x00
            private byte[] OpCode = {0x00, 0x50}; // Eqivalent to  0x5000. Transmitted low byte first.
            private byte[] ProtVer = {0x00, 0x0E}; // Protocol revision number 14.
            private byte Sequence = 0x00;
            private byte Phisical = 0x00;
            private byte[] Universe = {0x00, 0x00};    // Equivalent to Universe 0
            private byte[] Length = {0x02, 0x00};      // Equivalent to decimal 512
            public byte[] Data;
            public byte[] BlackoutData;

            public double[] Channels = new double[512];
            public double Master = 0;

            //  0-7     =>ID
            //  8-9     =>OpCode
            //  10-11   =>ProtVer
            //  12      =>Sequence
            //  13      =>Phisical
            //  14-15   =>Universe
            //  16-17   =>Length
            //  18-530  =>Data  

            public ArtDmx()
            {
                InicializeData(ref Data);
                InicializeData(ref BlackoutData);
                for (var i = 0; i < Channels.Length; i++)
                    Channels[i] = 0;
                UpdateChannels();
            }

            void InicializeData(ref byte[] data)
            {
                data = new byte[530];
                Array.Copy(Id, 0, data, 0, Id.Length);
                Array.Copy(OpCode, 0, data, 8, OpCode.Length);
                Array.Copy(ProtVer, 0, data, 10, ProtVer.Length);
                data[12] = Sequence;
                data[13] = Phisical;
                Array.Copy(Universe, 0, data, 14, Universe.Length);
                Array.Copy(Length, 0, data, 16, Length.Length);
                for (var i = 18; i < Data.Length; i++)
                {
                    data[i] = 0x00;
                }
            }

            public void SetChannelValue(int channel, double value)
            {
                if (channel < 1 && channel > 512) return;
                var v = value;
                if (v < 0) v = 0;
                if (v > 255) v = 255;
                Channels[channel-1] = v;
                UpdateChannels();
            }

            public void SetMasterValue(double value)
            {
                var v = value;
                if (v < 0) v = 0;
                if (v > 255) v = 255;
                Master = v;
                UpdateChannels();
            }

            private void UpdateChannels()
            {
                for (var i = 0; i < Channels.Length; i++)
                {
                    var v = (int) (Channels[i]*Master/255);
                    Data[18 + i] = (byte) v;
                }
            }

        }
    }
}
