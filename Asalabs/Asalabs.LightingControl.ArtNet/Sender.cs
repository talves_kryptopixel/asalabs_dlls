﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Asalabs.Extensions;

namespace Asalabs.LightingControl.ArtNet
{
    internal class Sender : IDisposable
    {
        public event EventHandler<StringArgs> ErrorReceived; 
        private UdpClient UdpClient { set; get; }
        public IPAddress Ip { private set; get; }
        public int Port { private set; get; }
        public short Ttl { private set; get; }
        private IPEndPoint RemoteEp { set; get; }

        public Sender()
        {
            Ip = IPAddress.Broadcast;
            Port = 6454;
            Ttl = 8;
            UdpClient = new UdpClient {ExclusiveAddressUse = false, Ttl = Ttl};
            RemoteEp = new IPEndPoint(Ip, Port);
        }

        public void Send(byte[] data)
        {
            try
            {
                UdpClient.Send(data, data.Length, RemoteEp);
            }
            catch (Exception ex)
            {
                if (ErrorReceived != null)
                    ErrorReceived(this, new StringArgs {Value = ex.Message+"\n"+ex.StackTrace});
            }
        }

        public void Close()
        {
            if (UdpClient == null) return;
            UdpClient.Close();
        }

        public void Dispose()
        {
            Close();
            UdpClient = null;
        }
    }
}
