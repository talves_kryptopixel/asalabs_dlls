﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Asalabs.Extensions;

namespace Asalabs.LightingControl.ArtNet
{
    internal class Listener : IDisposable
    {
        public event EventHandler<StringArgs> ErrorReceived;
        public event EventHandler<DmxDataArgs> DataReceived;

        public IPAddress Ip { private set; get; }
        public int ListenPort { private set; get; }
        public bool IsListening { private set; get; }
        public bool IsClosing { private set; get; }

        private UdpClient UdpClient { set; get; }

        private IPEndPoint _localEp;

        private volatile bool _done = false;


        public class DataArgs : EventArgs
        {
            public string Data { set; get; }
        }

        private System.Threading.Thread _thread;

        public void StartListening()
        {
            try
            {
                if (IsListening) return;

                Ip = IPAddress.Any;
                ListenPort = 6454;

                UdpClient = new UdpClient(ListenPort);
                _localEp = new IPEndPoint(IPAddress.Any, ListenPort);
                _thread = new System.Threading.Thread(Receive) {IsBackground = true};
                _thread.Start();
                IsClosing = false;
            }
            catch (Exception ex)
            {
                if (ErrorReceived != null)
                    ErrorReceived(this, new StringArgs {Value = ex.Message + "\n" + ex.StackTrace});
            }
        }

        private void Receive()
        {
            try
            {
                while (!_done)
                {
                    var bytes = UdpClient.Receive(ref _localEp);
                    if (bytes.Length > 0)
                    {
                        if (bytes.Length > 18)
                        {
                            var data = new DmxDataArgs();
                            for(int i=1;i<bytes.Length-18+1;i++)
                            {
                                var dc = new DmxChannel()
                                {
                                    Channel = i,
                                    Value = (int)bytes[18 + i-1]
                                };
                                data.Data.Add(dc);
                                if(dc.Channel==1)
                                    continue;
                            }

                       
                        if (DataReceived != null)
                           DataReceived(this,data);
                        }
                    }
                    IsListening = true;
                    //Console.WriteLine("Received broadcast from {0} :\n {1}\n",groupEP.ToString(),Encoding.ASCII.GetString(bytes, 0, bytes.Length));
                }

            }
            catch (Exception ex)
            {
                if (!IsClosing)
                    Receive();
            }
        }

        public void StopListening()
        {
            _done = true;
            if (UdpClient != null)
                UdpClient.Close();
            if (_thread != null)
                _thread.Abort();
        }
        public void Dispose()
        {
            IsClosing = true;
            StopListening();
            UdpClient = null;
        }
    }
}
