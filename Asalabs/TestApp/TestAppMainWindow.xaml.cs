﻿using System;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Input;
using Asalabs.ControlProtocol.Sony;
using Asalabs.Extensions;
using Asalabs.Windows;
using Asalabs.Net;
using MessageBox = Asalabs.Windows.MessageBox;

namespace TestApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        readonly TimeoutTimer _timeoutTimer = new TimeoutTimer(2000);
        private readonly LogWindow _log = new LogWindow();

        public MainWindow()
        {
            InitializeComponent();
            Extensions.ValidateMacAddressChars("t");
            PreviewMouseDoubleClick += MainWindow_PreviewMouseDoubleClick;
            _log.Show();
            var writer = new TextBoxStreamWriter();
            writer.TextReceived += writer_TextReceived;
            Console.SetOut(writer);
            var gea = NetworkInformation.GetGigabitEthernetMacAddressList();
            foreach (var g in gea)
                _log.Write("MacAddress : " + g.ToFormatedString());

            _timeoutTimer.TimeOutEnded += TimeOutCounter_TimeOutEnded;
        }


        void TimeOutCounter_TimeOutEnded(object sender, System.Timers.ElapsedEventArgs e)
        {
            Action showMessage = (() => { 
             MessageBox.Show("Loaded!\nTeste lçkijçwe qç ijqw epçoij qwei jqweoi jqw+eo riqjw+eorij qwe+roi jqwe+r oiqjwe+ oijqw er+oiqwjer +oqwiejr +qoiwejr q+woie jrq+woeir jq+weo irjq+woeirj q+woeir jq+woeirjq+woei jrq+owier jq+woei rjq+woei jrq+woe rijqw+eoi", "new title");
            });
            Dispatcher.BeginInvoke(showMessage);
        }

        void writer_TextReceived(object sender, StringArgs e)
        {
            _log.Write(e.Value);
        }

        void MainWindow_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            _timeoutTimer.Start();
        }

        public class TextBoxStreamWriter : TextWriter
        {
            public event EventHandler<StringArgs> TextReceived;

            public override void Write(char value)
            {
                base.Write(value);
                SendText(value.ToString()); // When character data is written, append it to the text box.
            }

            private void SendText(string s)
            {
                if (TextReceived != null)
                    TextReceived(null, new StringArgs {Value = s});
            }

            public override Encoding Encoding
            {
                get { return System.Text.Encoding.UTF8; }
            }
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            _log.Close();
            base.OnClosing(e);
        }

    }
}
