﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Asalabs.LigthingControl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class ChannelValuePanel : UserControl
    {
        private int _value;
        private int _channel;

        public ChannelValuePanel()
        {
            InitializeComponent();
        }

        public int Channel
        {
            set
            {
                _channel = value;
                TbxChannel.Text = _channel.ToString();
            }
            get { return _channel; }
        }

        public int Value
        {
            set
            {
                _value = value;
                TbxValue.Text = _value.ToString();
            }
            get { return _value; }
        }

        public int PercentageValue { get { return _value/255; } }
    }
}
