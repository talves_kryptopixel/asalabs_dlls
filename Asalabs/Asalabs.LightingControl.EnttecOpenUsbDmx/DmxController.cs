﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Asalabs.Extensions;

namespace Asalabs.LightingControl.EnttecOpenUsbDmx
{
    public class DmxController
    {
        public event EventHandler<StringArgs> StatusChanged;
        public OpenDmx OpenDmx = new OpenDmx();

        public DmxController()
        {
            try
            {
                OpenDmx.Start(); //find and connect to devive (first found if multiple)
                switch (OpenDmx.Status)
                {
                    case FT_STATUS.FT_DEVICE_NOT_FOUND:
                        Trigger_StatusChanged("No Enttec USB Device Found");
                        break;
                    case FT_STATUS.FT_OK:
                        Trigger_StatusChanged("Found DMX on USB");
                        break;
                    default:
                        Trigger_StatusChanged("Error Opening Device");
                        break;
                }
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp);
                Trigger_StatusChanged("Error Connecting to Enttec USB Device");

            }
        }

        public void SetAllChannelsOn()
        {
            if (OpenDmx.Status != FT_STATUS.FT_OK)
            {
                Trigger_StatusChanged(OpenDmx.Status.ToString());
                return;
            }
            for (Int16 i = 1; i <= 512; i++)
                OpenDmx.SetDmxValue(i, Convert.ToByte(255));
        }

        public void SetAllChannelsOff()
        {
            if (OpenDmx.Status != FT_STATUS.FT_OK)
            {
                Trigger_StatusChanged(OpenDmx.Status.ToString());
                return;
            }
            for (Int16 i = 1; i <= 512; i++)
                OpenDmx.SetDmxValue(i, Convert.ToByte(0));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel">Dmx Channel. Must be a value between 1 and 512. If not the command is discarded.</param>
        /// <param name="value">Dmx channel value. Must be a value between 0 and 255. Values less than 0 will be 0. Values more than 255 will be 255.</param>
        public void SetChannelValue(Int16 channel, double value)
        {
            if (OpenDmx.Status != FT_STATUS.FT_OK)
            {
                Trigger_StatusChanged(OpenDmx.Status.ToString());
                return;
            }
            var v = value;
            if (v < 0) v = 0;
            if (v > 255) v = 255;
            if (channel >= 1 && channel <= 512)
                OpenDmx.SetDmxValue(channel, v);
        }

        public void SetMasterValue(double value)
        {
            if (OpenDmx.Status != FT_STATUS.FT_OK)
            {
                Trigger_StatusChanged(OpenDmx.Status.ToString());
                return;
            }
            var v = value;
            if (v < 0) v = 0;
            if (v > 255) v = 255;
            OpenDmx.SetMasterDmxValue(v);
        }

        private void Trigger_StatusChanged(string message)
        {
            if (StatusChanged != null)
                StatusChanged(this, new StringArgs {Value = message});
        }
    }
}
