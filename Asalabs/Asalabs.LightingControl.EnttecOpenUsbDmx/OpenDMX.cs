using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.IO;
using System.Threading;


namespace Asalabs.LightingControl.EnttecOpenUsbDmx
{

    public class OpenDmx :IDisposable

    {
        public bool Blackout { set; get; }
        public bool Active { set; get; }
        public double Master { private set; get; }
        private Thread _backGroundThread;
        public volatile double[] ChannelsValues = new double[513];
        public volatile byte[] Buffer = new byte[513];
        public volatile byte[] BufferBlackout = new byte[513];
        public static uint Handle;
        public static bool Done = false;
        public static int BytesWritten = 0;
        public static FT_STATUS Status;

        public const byte Bits8 = 8;
        public const byte StopBits2 = 2;
        public const byte ParityNone = 0;
        public const UInt16 FlowNone = 0;
        public const byte PurgeRx = 1;
        public const byte PurgeTx = 2;
  

        [DllImport("FTD2XX.dll")]
        public static extern FT_STATUS FT_Open(UInt32 uiPort, ref uint ftHandle);
        [DllImport("FTD2XX.dll")]
        public static extern FT_STATUS FT_Close(uint ftHandle);
        [DllImport("FTD2XX.dll")]
        public static extern FT_STATUS FT_Read(uint ftHandle, IntPtr lpBuffer, UInt32 dwBytesToRead, ref UInt32 lpdwBytesReturned);
        [DllImport("FTD2XX.dll")]
        public static extern FT_STATUS FT_Write(uint ftHandle, IntPtr lpBuffer, UInt32 dwBytesToRead, ref UInt32 lpdwBytesWritten);
        [DllImport("FTD2XX.dll")]
        public static extern FT_STATUS FT_SetDataCharacteristics(uint ftHandle, byte uWordLength, byte uStopBits, byte uParity);
        [DllImport("FTD2XX.dll")]
        public static extern FT_STATUS FT_SetFlowControl(uint ftHandle, char usFlowControl, byte uXon, byte uXoff);
        [DllImport("FTD2XX.dll")]
        public static extern FT_STATUS FT_GetModemStatus(uint ftHandle, ref UInt32 lpdwModemStatus);
        [DllImport("FTD2XX.dll")]
        public static extern FT_STATUS FT_Purge(uint ftHandle, UInt32 dwMask);
        [DllImport("FTD2XX.dll")]
        public static extern FT_STATUS FT_ClrRts(uint ftHandle);
        [DllImport("FTD2XX.dll")]
        public static extern FT_STATUS FT_SetBreakOn(uint ftHandle);
        [DllImport("FTD2XX.dll")]
        public static extern FT_STATUS FT_SetBreakOff(uint ftHandle);
        [DllImport("FTD2XX.dll")]
        public static extern FT_STATUS FT_GetStatus(uint ftHandle, ref UInt32 lpdwAmountInRxQueue, ref UInt32 lpdwAmountInTxQueue, ref UInt32 lpdwEventStatus);
        [DllImport("FTD2XX.dll")]
        public static extern FT_STATUS FT_ResetDevice(uint ftHandle);
        [DllImport("FTD2XX.dll")]
        public static extern FT_STATUS FT_SetDivisor(uint ftHandle, char usDivisor);


        public void Start()
        {
            Handle = 0;
            Status = FT_Open(0, ref Handle);
            SetDmxValue(0, 0);  //Set DMX Start Code
            _backGroundThread = new Thread(WriteData){IsBackground = true,Priority = ThreadPriority.Lowest};
            _backGroundThread.Start();
        }

        public void SetDmxValue(int channel, double value)
        {
            if (ChannelsValues != null)
            {
                if (channel >= 1 && channel <= 512)
                    ChannelsValues[channel] = value;
            }
            UpdateValues();
        }
        public void SetMasterDmxValue(double value)
        {
            Master = value;
            UpdateValues();
        }

        private void UpdateValues()
        {
            for (var i = 1; i < ChannelsValues.Length; i++)
            {
                var res = ChannelsValues[i]*(Master/255);
                Buffer[i] = Convert.ToByte(res);
            }
        }

        private void WriteData()
        {
            while (!Done)
            {
                if (Active)
                {
                    byte[] buffer;
                    if (Blackout)
                    {
                        buffer = new byte[BufferBlackout.Length];
                        Array.Copy(BufferBlackout, buffer, BufferBlackout.Length);
                    }
                    else
                    {
                        buffer = new byte[Buffer.Length];
                        Array.Copy(Buffer, buffer, Buffer.Length);
                    }

                    InitOpenDmx();
                    FT_SetBreakOn(Handle);
                    FT_SetBreakOff(Handle);
                    BytesWritten = Write(Handle, buffer, buffer.Length);
                }
                Thread.Sleep(20);
            }
        }

        public int Write(uint handle, byte[] data, int length)
  {
            IntPtr ptr = Marshal.AllocHGlobal((int)length);
            Marshal.Copy(data, 0, ptr, (int)length);
            uint bytesWritten = 0;
            Status = FT_Write(handle, ptr, (uint)length, ref bytesWritten);
            return (int)bytesWritten;
        }

        public void InitOpenDmx()
        {
            Status = FT_ResetDevice(Handle);
            Status = FT_SetDivisor(Handle, (char)12);  // set baud rate
            Status = FT_SetDataCharacteristics(Handle, Bits8, StopBits2, ParityNone);
            Status = FT_SetFlowControl(Handle, (char)FlowNone, 0, 0);
            Status = FT_ClrRts(Handle);
            Status = FT_Purge(Handle, PurgeTx);
            Status = FT_Purge(Handle, PurgeRx);
        }

        public void Dispose()
        {   
            _backGroundThread.Abort();
        }
    }

    /// <summary>
    /// Enumaration containing the varios return status for the DLL functions.
    /// </summary>
    public enum FT_STATUS
    {
        FT_OK = 0,
        FT_INVALID_HANDLE,
        FT_DEVICE_NOT_FOUND,
        FT_DEVICE_NOT_OPENED,
        FT_IO_ERROR,
        FT_INSUFFICIENT_RESOURCES,
        FT_INVALID_PARAMETER,
        FT_INVALID_BAUD_RATE,
        FT_DEVICE_NOT_OPENED_FOR_ERASE,
        FT_DEVICE_NOT_OPENED_FOR_WRITE,
        FT_FAILED_TO_WRITE_DEVICE,
        FT_EEPROM_READ_FAILED,
        FT_EEPROM_WRITE_FAILED,
        FT_EEPROM_ERASE_FAILED,
        FT_EEPROM_NOT_PRESENT,
        FT_EEPROM_NOT_PROGRAMMED,
        FT_INVALID_ARGS,
        FT_OTHER_ERROR
    };

}