﻿using System;
using System.Net.Sockets;

namespace Asalabs.Net.Tcp
{
    public class SocketPacket : IDisposable
    {
        public SocketPacket()
        {

        }
        
        public SocketPacket(Socket socket)
        {
            CurrentSocket = socket;
        }

        // Constructor which takes a Socket and a client number
        public SocketPacket(Socket socket, int clientNumber)
        {
            CurrentSocket = socket;
            ClientNumber = clientNumber;
        }


        public Socket CurrentSocket { set; get; }
        public int ClientNumber { set; get; }
        // Buffer to store the data sent by the client
        byte[] _dataBuffer = new byte[5000];
        public byte[] DataBuffer { set { _dataBuffer = value; } get { return _dataBuffer; } }
        public string StringDataBuffer { set; get; }
        
        #region Dispose
        private bool disposed = false;
        //Implement IDisposable.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // Free other state (managed objects).
                    CurrentSocket.Shutdown(System.Net.Sockets.SocketShutdown.Both);
                    CurrentSocket.Close();
                    CurrentSocket = null;
                    StringDataBuffer = null;
                }
                // Free your own state (unmanaged objects).
                // Set large fields to null.
                DataBuffer = null;
                disposed = true;
            }
        }

        // Use C# destructor syntax for finalization code.
        ~SocketPacket()
        {
            Dispose(false);
        }
        #endregion
    }
}