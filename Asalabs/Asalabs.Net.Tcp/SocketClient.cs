﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace Asalabs.Net.Tcp
{
    public class SocketClient
    {
        public event EventHandler<DataArgs> DataReceived;
        public event EventHandler<DataArgs> InfoAvailable;

        private const char STX = (char)2;
        private const char ETX = (char)3;

        public class DataArgs : EventArgs
        {
            public string Data { set; get; }
        }

        IPAddress _ip = IPAddress.Parse("127.0.0.1");
        public IPAddress IP { set { _ip = value; if (AutoConnect) Reconnect(); } get { return _ip; } }
        Int16 _port = 8000;
        public Int16 Port { set { _port = value; if (AutoConnect) Reconnect(); } get { return _port; } }

        bool _autoConnect = false;
        public bool AutoConnect { get { return _autoConnect; } set{_autoConnect = value; EnableAutoConnect(value); }}

        System.Timers.Timer timer = new System.Timers.Timer(1000);

        void EnableAutoConnect(bool enable)
        {
            if (enable)
            {
                timer.AutoReset = true;
                timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Elapsed);
                timer.Enabled = true;
                timer.Start();
            }
            else
            {
                timer.AutoReset = true;
                timer.Elapsed -= new System.Timers.ElapsedEventHandler(timer_Elapsed);
                timer.Enabled = false;
                timer.Stop();
            }
        }

        byte[] my_dataBuffer = new byte[10];

        IAsyncResult my_result;
        
        public AsyncCallback my_pfnCallBack;
        
        public Socket clientSocket;

        void  Reconnect()
        {
            Disconnect();
            Connect();
        }

        public SocketClient()
        {
        }

        public SocketClient(IPAddress ip, Int16 port)
        {
            IP = ip;
            Port = port;
        }

        void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (clientSocket != null){
                if (!clientSocket.Connected)
                    Connect();
            }
            else
                Connect();
        }

        void CloseSocket()
        {
            try
            {
                if (clientSocket != null)
                {
                    clientSocket.Close();
                    clientSocket = null;
                }
            }
            catch (Exception)
            {
            }
        }

        private bool Connect()
        {
            try
            {
                // Create the socket instance
                clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                // Create the end point
                IPEndPoint ipEnd = new IPEndPoint(IP, Port);
                // Connect to the remote host
                clientSocket.Connect(ipEnd);
                if (clientSocket.Connected)
                {

                    //Wait for data asynchronously 
                    WaitForData();
                    return true;
                }
                else
                    return false;
            }
            catch (SocketException)
            {
                return false;
            }
        }
        
        public void Send(string msg)
        {
            // Use the following code to send bytes
            byte[] byData = System.Text.Encoding.ASCII.GetBytes(STX + msg + ETX);
            Send(byData);
        }

        private void Send(byte[] data)
        {
            try
            {
                if (clientSocket != null)
                {
                    if (!clientSocket.Connected)
                        Connect();
                    if (clientSocket.Connected)
                    {
                        clientSocket.Send(data);
                    }
                }
            }
            catch (SocketException)
            {
            }
        }

        public void WaitForData()
        {
            try
            {
                if (my_pfnCallBack == null)
                {
                    my_pfnCallBack = new AsyncCallback(OnDataReceived);
                }
                SocketPacket theSocPkt = new SocketPacket();
                theSocPkt.CurrentSocket = clientSocket;
                // Start listening to the data asynchronously
                my_result = clientSocket.BeginReceive(theSocPkt.DataBuffer, 0, theSocPkt.DataBuffer.Length,
                                                            SocketFlags.None, my_pfnCallBack, theSocPkt);
            }
            catch (SocketException)
            {
            }

        }
        
        public void OnDataReceived(IAsyncResult asyn)
        {
            try
            {
                SocketPacket theSockId = (SocketPacket)asyn.AsyncState;
                int iRx = theSockId.CurrentSocket.EndReceive(asyn);
                char[] chars = new char[iRx + 1];
                System.Text.Decoder d = System.Text.Encoding.UTF8.GetDecoder();
                int charLen = d.GetChars(theSockId.DataBuffer, 0, iRx, chars, 0);
                System.String szData = new System.String(chars);
                if (szData == "\0")
                {
                    theSockId.CurrentSocket.Disconnect(true);
                }
                else
                {
                    Process(szData);
                }
            }
            catch (ObjectDisposedException)
            {
                System.Diagnostics.Debugger.Log(0, "1", "\nOnDataReceived: Socket has been closed\n");
            }
            catch (SocketException)
            {
            }
            catch (Exception)
            {
            }
            finally
            {
                WaitForData();
            }
        }

        static private string _rxBuffer = string.Empty;
        void Process(string data)
        {
            _rxBuffer += data;
            if (_rxBuffer.Contains(STX) && _rxBuffer.Contains(ETX))
            {
                var idxStx = _rxBuffer.IndexOf(STX);
                var idxEtx = _rxBuffer.IndexOf(ETX);

                if (idxEtx > idxStx)
                {
                    if (DataReceived != null)
                        DataReceived(this, new DataArgs {Data = _rxBuffer.Substring(idxStx + 1, idxEtx - idxStx - 1)});

                }
                _rxBuffer = _rxBuffer.Substring(idxEtx + 1);
            }

        }

        private void Disconnect()
        {
            if (clientSocket != null)
            {
                clientSocket.Close();
                clientSocket = null;
            }
        }

        private void Write(string str)
        {
            if (InfoAvailable != null)
                InfoAvailable(this, new DataArgs { Data = str });
        }

    }
}