﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Asalabs.Net.Tcp
{
    public class SocketServer
    {

        public event EventHandler<DataArgs> DataReceived;
        public event EventHandler<DataArgs> InfoAvailable;

        public class DataArgs : EventArgs
        {
            public int Client { set; get; }
            public string Data { set; get; }
        }

        public AsyncCallback pfnWorkerCallBack;

        private Socket m_mainSocket;

        // An ArrayList is used to keep track of worker sockets that are designed
        // to communicate with each connected client. Make it a synchronized ArrayList
        // For thread safety
        private System.Collections.ArrayList socketDataList;

        // The following variable will keep track of the cumulative 
        // total number of clients connected at any time. Since multiple threads
        // can access this variable, modifying this variable should be done
        // in a thread safe manner
        private int m_clientCount = 0;

        bool _listening = false;

        private Int16 _port = 8000;

        public Int16 Port
        {
            get { return _port; }
            set
            {
                _port = value;
                if (_listening)
                {
                    StopListen();
                    StartListen();
                }
            }
        }

        public SocketServer()
        {
            socketDataList = ArrayList.Synchronized(new System.Collections.ArrayList());
        }

        public void StartListen()
        {
            try
            {
                socketDataList = ArrayList.Synchronized(new System.Collections.ArrayList());
                //int port = System.Convert.ToInt32(portStr);
                // Create the listening socket...
                m_mainSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                IPEndPoint ipLocal = new IPEndPoint(IPAddress.Any, _port);
                // Bind to local IP Address...
                m_mainSocket.Bind(ipLocal);
                // Start listening...
                m_mainSocket.Listen(4);
                // Create the call back for any client connections...
                m_mainSocket.BeginAccept(new AsyncCallback(OnClientConnect), null);
                UpdateControls(true);

            }
            catch (SocketException se)
            {
                Write("StartListen : " + se.Message);
            }
            catch (Exception ex)
            {
                Write("StartListen : " + ex.Message);
            }

        }

        private void UpdateControls(bool listening)
        {
            this._listening = listening;
        }

        // This is the call back function, which will be invoked when a client is connected
        void OnClientConnect(IAsyncResult asyn)
        {
            try
            {
                // Here we complete/end the BeginAccept() asynchronous call
                // by calling EndAccept() - which returns the reference to
                // a new Socket object
                Socket workerSocket = m_mainSocket.EndAccept(asyn);

                // Now increment the client count for this client 
                // in a thread safe manner
                Interlocked.Increment(ref m_clientCount);

                SocketPacket newSocketPacket = new SocketPacket(workerSocket, m_clientCount);

                // Add the workerSocket reference to our ArrayList
                socketDataList.Add(newSocketPacket);

                // Send a welcome message to client
                SendMsgToClient("Welcome to Socket Server!\r\n", m_clientCount);

                // Update the list box showing the list of clients (thread safe call)
                UpdateClientList();

                // Let the worker Socket do the further processing for the 
                // just connected client
                WaitForData(newSocketPacket);

                // Since the main Socket is now free, it can go back and wait for
                // other clients who are attempting to connect
                m_mainSocket.BeginAccept(new AsyncCallback(OnClientConnect), null);
            }
            catch (ObjectDisposedException)
            {
                Write("OnClientConnection: Socket has been closed\n");
            }
            catch (SocketException se)
            {
                Write("OnClientConnect : " + se.Message);
            }
            catch (Exception ex)
            {
                Write("OnClientConnect : " + ex.Message);
            }

        }

        // Start waiting for data from the client
        void WaitForData(SocketPacket sp)
        {
            try
            {
                if (pfnWorkerCallBack == null)
                {
                    // Specify the call back function which is to be 
                    // invoked when there is any write activity by the 
                    // connected client
                    pfnWorkerCallBack = new AsyncCallback(OnDataReceived);
                }

                sp.CurrentSocket.BeginReceive(sp.DataBuffer, 0, sp.DataBuffer.Length, SocketFlags.None, pfnWorkerCallBack, sp);
            }
            catch (SocketException se)
            {
                Write("WaitForData : " + se.Message);
            }
            catch (Exception ex)
            {
                Write("WaitForData : " + ex.Message);
            }
        }

        // This the call back function which will be invoked when the socket
        // detects any client writing of data on the stream
        void OnDataReceived(IAsyncResult asyn)
        {
            SocketPacket socketData = (SocketPacket)asyn.AsyncState;
            try
            {
                // Complete the BeginReceive() asynchronous call by EndReceive() method
                // which will return the number of characters written to the stream 
                // by the client
                int iRx = socketData.CurrentSocket.EndReceive(asyn);
                char[] chars = new char[iRx + 1];
                // Extract the characters as a buffer
                System.Text.Decoder d = System.Text.Encoding.UTF8.GetDecoder();
                int charLen = d.GetChars(socketData.DataBuffer, 0, iRx, chars, 0);

                string szData = new string(chars);

                if (szData == "\0")
                    socketData.CurrentSocket.Disconnect(true);
                else
                {
                    Process(socketData, szData);
                }
            }
            catch (ObjectDisposedException)
            {
                System.Diagnostics.Debugger.Log(0, "1", "\nOnDataReceived: Socket has been closed\n");
            }
            catch (SocketException se)
            {
                if (se.ErrorCode == 10054) // Error code for Connection reset by peer
                {
                    string msg = "Client " + socketData.ClientNumber + " Disconnected" + "\n";
                    Write(msg);

                    // Remove the reference to the worker socket of the closed client
                    // so that this object will get garbage collected
                    socketDataList[socketData.ClientNumber - 1] = null;
                    UpdateClientList();
                }
                else
                {
                    Write("OnDataReceived : " + se.Message);
                }
            }
            catch (Exception ex)
            {
                Write("OnDataReceived : " + ex.Message);
            }
            finally
            {
                // Continue the waiting for data on the Socket
                WaitForData(socketData);
            }
        }

        private const char STX = (char)2;
        private const char ETX = (char)3;
        void Process(SocketPacket client, string data)
        {
            client.StringDataBuffer += data;
            if (client.StringDataBuffer.Contains(STX.ToString()) && client.StringDataBuffer.Contains(ETX.ToString()))
            {
                var idxStx = client.StringDataBuffer.IndexOf(STX);
                var idxEtx = client.StringDataBuffer.IndexOf(ETX);

                if (idxEtx > idxStx)
                {
                    if (DataReceived != null)
                        DataReceived(this, new DataArgs { Client = client.ClientNumber, Data = client.StringDataBuffer.Substring(idxStx+1, idxEtx - idxStx-1) });

                    client.StringDataBuffer = client.StringDataBuffer.Substring(idxEtx + 1);
                }
            }

        }

        public void StopListen()
        {
            CloseSockets();
            UpdateControls(false);
        }

        void CloseSockets()
        {
            if (m_mainSocket != null)
            {
                m_mainSocket.Close();
            }
            for (int i = 0; i < socketDataList.Count; i++)
            {
                if (((SocketPacket)socketDataList[i]) != null)
                    if (((SocketPacket)socketDataList[i]).CurrentSocket != null)
                        ((SocketPacket)socketDataList[i]).CurrentSocket.Close();
            }
        }

        List<string> ClientList = new List<string>();

        // Update the list of clients that is displayed
        void UpdateClientList()
        {
            try
            {
                lock (ClientList)
                {
                    ClientList.Clear();
                    for (int i = 0; i < socketDataList.Count; i++)
                    {
                        if (((SocketPacket)socketDataList[i]) != null)
                            if (((SocketPacket)socketDataList[i]).CurrentSocket != null)
                            {
                                if (((SocketPacket)socketDataList[i]).CurrentSocket.Connected)
                                {
                                    ClientList.Add(Convert.ToString(i + 1));
                                }
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                Write("UpdateClientList : \n" + ex.Message);
            }
        }
        
        public void SendMsgToClient(string data, int clientNumber)
        {
            //if (string.IsNullOrEmpty(data)) return;
            try
            {
                if (((SocketPacket)socketDataList[clientNumber - 1]) != null)
                    if (((SocketPacket)socketDataList[clientNumber - 1]).CurrentSocket != null)
                    {
                        // Convert the reply to byte array
                        ((SocketPacket) socketDataList[clientNumber - 1]).CurrentSocket.Send(
                            System.Text.Encoding.UTF8.GetBytes(STX + data + ETX));
                    }
            }
            catch (Exception ex)
            {
                Write("SendMsgToClient : \n" + ex.Message);
            }
        }

        public void SendMsgBroadcast(string data)
        {
            //if (string.IsNullOrEmpty(data)) return;
            try
            {
                for (int clientNumber = 0; clientNumber < socketDataList.Count; clientNumber++)
                {
                    if (((SocketPacket)socketDataList[clientNumber]) != null)
                        if (((SocketPacket)socketDataList[clientNumber]).CurrentSocket != null)
                        {
                            if (((SocketPacket)socketDataList[clientNumber]).CurrentSocket.Connected)
                            {
                                // Convert the reply to byte array
                                ((SocketPacket) socketDataList[clientNumber]).CurrentSocket.Send(
                                    System.Text.Encoding.UTF8.GetBytes(STX + data + ETX));
                            }
                        }
                }
                Write("SendMsgBroadcast : " + data);
            }
            catch (Exception ex)
            {
                Write("SendMsgBroadcast : \n" + ex.Message);
            }
        }


        private void Write(string str)
        {
            if (InfoAvailable != null)
                InfoAvailable(this, new DataArgs {Data = str});
        }
    }
}
