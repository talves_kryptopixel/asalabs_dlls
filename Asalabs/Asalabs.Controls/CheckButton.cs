﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Asalabs.Drawing;

namespace Asalabs.Controls
{
    public class CheckButton : Button
    {
        bool _isChecked = false;
        public bool IsChecked { set { _isChecked = value; UpdateBackground(); } get { return _isChecked; } }

        Brush _checkedBrush = Brushes.WhiteSmoke;
        public Brush CheckedBrush { set { _checkedBrush = value; UpdateBackground(); } get { return _checkedBrush; } }

        Brush _uncheckedBrush = Brushes.Transparent;
        public Brush UncheckedBrush { set { _uncheckedBrush = value; UpdateBackground(); } get { return _uncheckedBrush; } }

        void UpdateBackground()
        {
            if (IsChecked)
                Background = _checkedBrush;
            else
                Background = _uncheckedBrush;
        }

        public CheckButton()
        {
            UpdateBackground();
            Focusable = false;
        }
    }

    public class ElementButton : CheckButton
    {
        public object Element;
    }

    public class CheckMenu : MenuItem
    {
        private new bool IsCheckable { set; get; }

        bool _isChecked = false;
        public new bool IsChecked { set { _isChecked = value; UpdateIcon(); } get { return _isChecked; } }
        readonly Image _checkedIcon = new Image();
        readonly Image _uncheckedIcon = new Image();
        
        void UpdateIcon()
        {
            if (IsChecked)
                Icon = _checkedIcon;
            else
                Icon = _uncheckedIcon;
        }

        public CheckMenu()
        {
            this.Click += new RoutedEventHandler(CheckMenu_Click);
            IsCheckable = false;
            _checkedIcon.Source = Properties.Resources.checked_26.ToImageSource();
            _checkedIcon.Height = _checkedIcon.Width = 12;
            _uncheckedIcon.Source = Properties.Resources.unchecked_26.ToImageSource();
            _uncheckedIcon.Height = _uncheckedIcon.Width = 12;
            UpdateIcon();
        }

        void CheckMenu_Click(object sender, RoutedEventArgs e)
        {
            _isChecked = !_isChecked;
            UpdateIcon();
        }
    }

    public class RadioMenuItem : MenuItem
    {
        private new bool IsCheckable { set; get; }

        bool _isChecked = false;
        public new bool IsChecked { set { _isChecked = value; UpdateIcon(); } get { return _isChecked; } }
        readonly Image _checkedIcon = new Image();
        readonly Image _uncheckedIcon = new Image();
        RadioMenuItemGroup _group;
        public object Element;
        public RadioMenuItemGroup Group
        {
            set
            {
                _group = value;
                _group.Children.Add(this);
            }
            get { return _group; }
        } 
        
        void UpdateIcon()
        {
            if (IsChecked)
                Icon = _checkedIcon;
            else
                Icon = _uncheckedIcon;
        }

        public RadioMenuItem()
        {
            InicializeRadioMenuItem();
        }

        void InicializeRadioMenuItem()
        {
            this.Click += new RoutedEventHandler(RadioMenuItem_Click);
            IsCheckable = false;
            _checkedIcon.Source = Properties.Resources.checked_26.ToImageSource();
            _checkedIcon.Height = _checkedIcon.Width = 12;
            _uncheckedIcon.Source = Properties.Resources.unchecked_26.ToImageSource();
            _uncheckedIcon.Height = _uncheckedIcon.Width = 12;
            IsChecked = false;
        }

        public RadioMenuItem(RadioMenuItemGroup _Group)
        {
            InicializeRadioMenuItem();
            Group = _Group;
        }

        void RadioMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Group.Children.Select(this);
        }
    }

    public class RadioMenuItemGroup
    {
        public event EventHandler SelectionChanged;
        public children Children = new children();

        public RadioMenuItemGroup()
        {
            Children.SelectionChanged += new EventHandler(Children_SelectionChanged);
        }

        void Children_SelectionChanged(object sender, EventArgs e)
        {
            if (SelectionChanged != null)
                SelectionChanged(sender, new EventArgs());
        }
        public class children
        {
            public event EventHandler SelectionChanged;
            readonly List<RadioMenuItem> _children = new List<RadioMenuItem>();
            public void Add(RadioMenuItem item)
            {
                _children.Add(item);
            }
            public void Remove(RadioMenuItem item)
            {
                _children.Remove(item);
            }
            public void Select(RadioMenuItem item)
            {
                item.IsChecked = true;
                foreach (var rmItem in _children)
                    if (!Equals(rmItem, item))
                        rmItem.IsChecked = false;
                if (SelectionChanged != null)
                    SelectionChanged(item, new EventArgs());
            }
        }
    }
}
