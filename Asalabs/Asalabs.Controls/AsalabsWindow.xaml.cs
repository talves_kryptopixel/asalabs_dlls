﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Asalabs.Controls
{
    /// <summary>
    /// Interaction logic for BaseLogoUserControl.xaml
    /// </summary>
    public partial class BaseLogoUserControl : UserControl
    {
        public Visibility MadeInPortugal
        {
            set { LbMadeInPortugal.Visibility = value; }
            get { return LbMadeInPortugal.Visibility; }
        }

        public BaseLogoUserControl()
        {
            InitializeComponent();
        }

        private void Asalabs_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            Process.Start("http://www.asalabs.com");
            e.Handled = true;
        }
    }
}
